<!-- CSS FOR THIS PAGE -->
@section('css')

<style type="text/css">
a:hover{
  text-decoration: none;
  color: #000;
}
/* .dateAndName{
  display: inline-block;
} */
.post-wrapper {
  /*height: 238px;*/
  margin-bottom: 10px;
}
.post-title {
  font-weight: 600;
  color: #000;
  display: block; /* or inline-block */
  text-overflow: ellipsis;
  word-wrap: break-word;
  overflow: hidden;
  max-height: 2.6em;
  line-height: 1.3em;
  height: 2.6em;
}
.guteurlsBox h1 {
  font-weight: 600!important;
  color: #000!important;
  display: block!important;
  text-overflow: ellipsis!important;
  word-wrap: break-word!important;
  overflow: hidden!important;
  max-height: 2.6em!important;
  line-height: 1.3em!important;
  height: 2.6em!important;
  font-size: 1rem!important;
  margin-top: 33px!important;
}
.guteurlsTop, .guteurlsBottom {
  display: none!important;
}
.dateAndName{
  display: inline-block;
}
.post-title:hover {
  color: #d64c1e;
}
.block{
  display: inline-block;
}
.title-1 {
  color: #126dd0e6;
}
.socialShere{
  display: inline-block;
}
.social{
  display: inline-block;
  width: 100px;
  height: 100px;
}
.description {
  /* display: inline-block; */
  line-height:1.5em;
  min-height:4.5em;
  max-height:4.5em;

  /* These are technically the same, but use both */
  overflow-wrap: break-word;
  word-wrap: break-word;

  -ms-word-break: break-all;
  /* This is the dangerous one in WebKit, as it breaks things wherever */
  word-break: break-all;
  /* Instead use this non-standard one: */
  word-break: break-word;

  /* Adds a hyphen where the word breaks, if supported (No Blink) */
  -ms-hyphens: auto;
  -moz-hyphens: auto;
  -webkit-hyphens: auto;
  hyphens: auto;
}

.row.footerLogoBar {
  /*margin-top: 250px;*/
}
</style>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
@stop
<div>
<div class="row">
  @foreach ($related as $post)
    <div class="col-md-3 post-wrapper" style="margin-bottom: 150px; top: 5px;">
      <div class="img-container post-container">
        <a href="{{url('post/'.$post->id)}}">
          @if ($post->type == 'image')
          <img src="{{ $post->img_path ? asset((file_exists($post->img_path) ? $post->img_path : 'assets/front/images/no_image.png') ): asset('/assets/front/images/no_image.png') }}" alt="post-image" width="100%">
          @endif

          @if ($post->type == 'video')
            <!-- <iframe src="{{ asset($post->video_path) }}" autoplay=false></iframe> -->
            <video src="{{ asset($post->video_path) }}" controls="true" height="238px"></video>
          @endif
        </a>
      </div>

      <div>
      <p class="py-2 mb-0 title-p">
        @if($post->idea_of_the_week == 1)
          <span class="title-red">Idea of the week</span>
        @endif
        <span class="title-1">{{$post->category->name ?? ''}}</span>
      </p>
      <a href="{{url('post/'.$post->id)}}">
        <h6 class="title-2 post-title">{{$post->title}}</h6>
      </a>
      <div class="description text-left">
        {{substr($post->content,0,75)}}
        <a href="{{url('post/'.$post->id)}}"><br>
          <span class="btn-readMore">Read More &#10148;</span>
        </a>
      </div> 
    <div style="position: absolute; bottom: 0; top: 390px;" class="">
    <div class="dateAndName" style="display: flex;">
        <span>{{date("jS F Y", strtotime($post->created_at))}}</span>
        <span>by {{$post->addedUser->first_name.' '.$post->addedUser->last_name}}</span>
        <a href="{{url('post/'.$post->id).'?comment=1'}}">
            <span> • Comments</span>
        </a>
    </div>
    
    <div class="social">
        {{--<div class='starrr' id='{{$post->id}}'></div>--}}
        <div style="display: inline-block;" class="rateYo" id="{{$post->id}}" data-rateyo-rating="{{ $post->ratings->avg('value') ?? 0}}"></div>
        <span style="font-size: 12px">({{ $post->ratings->count('value') ?? 0}} vote(s), average {{ round($post->ratings->avg('value'),1) ?? 0}} out of 5)</span>
    </div>
    <div>
        <ul class="socialShere">
            <li>
                <a onclick="socialShare(event)">
                    <button class="btn btn-share btn-fb" type="button" name="button" id='fb' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                        <i class="fab fa-facebook-f"></i>   Share  {{--   {{$post->share->where('type',1)->isEmpty()?'':$post->share->where('type',1)[0]->value}} --}}
                    </button>
                </a>
            </li>
            <li>
                <a onclick="socialShare(event)">
                    <button class="btn btn-share btn-tw" type="button" name="button" id='tw' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                        <i class="fab fa-twitter"></i>  Tweet
                    </button>
                </a>
            </li>
            <li>
                <a onclick="socialShare(event)">
                    <button class="btn btn-share btn-in" type="button" name="button" id='in' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}"">
                        <i class="fab fa-linkedin-in"></i> Share
                    </button>
                </a>
            </li>
            <li>
                <a onclick="socialShare(event)" href="{{'mailto:?subject=Ideaspies : '.$post->title.'&body=Idea Link : '.url('post/'.$post->id)}}" class="btn btn-share btnShareEmail"
                   title="Share by Email" id="email" post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                    <i class="fa fa-envelope" aria-hidden="true"></i> Email
                </a>
            </li>
        </ul>
    </div>
    </div>
    </div>
    </div>
  @endforeach
</div>

</div>

<!-- JS FOR THIS PAGE -->
@section('js')
{{-- for linked in share btn  --}}
<script async src="https://static.addtoany.com/menu/page.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script src="//unpkg.com/jscroll/dist/jquery.jscroll.min.js"></script>
  <script type="text/javascript">
  $(function () {
    $(".rateYo").rateYo();
    $(".rateYo").rateYo("option", "starWidth", "13px");
    $(".rateYo").rateYo("option", "fullStar", true);
  });
  var token = "{{ csrf_token() }}";
  $(".rateYo").rateYo()
    .on("rateyo.set", function (e, data) {
        $.post('post-rate',{id : e.currentTarget.id, value : data.rating, _token : token})
    });
  @if(!Session::has('regModal') && Sentinel::check() == false)
      window.setTimeout(function () {
          $('#regModal').modal()
      },10000)
    {{Session::put('regModal',true)}}
  @endif
    $('ul.pagination').hide();
  $(function() {
      $('.infinite-scroll').jscroll({
          autoTrigger: true,
          loadingHtml: '<div class="row justify-content-center"><img class="center-block" src="{{url('assets/front/images/load.gif')}}" alt="Loading..." /></div>',
          padding: 0,
          nextSelector: '.pagination li.active + li a',
          contentSelector: 'div.infinite-scroll',
          callback: function() {
              $('ul.pagination').remove();
              $(function () {
                    $(".rateYo").rateYo();
                    $(".rateYo").rateYo("option", "starWidth", "13px");
                    $(".rateYo").rateYo("option", "fullStar", true);
                });
                var token = "{{ csrf_token() }}";
                $(".rateYo").rateYo()
                    .on("rateyo.set", function (e, data) {
                        $.post('post-rate',{id : e.currentTarget.id, value : data.rating, _token : token})
                    });
          }
      });
  });
  </script>
    <script>
        function deletePost(post,e)
        {
            e.preventDefault();
            Swal.fire({
                title: 'Are you sure want to remove the post?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, remove it!'
            }).then((result) => {
                if (result.value) { 
                    fetch("{{url('post/delete').'/'}}"+post)
                    .then(response => {
                        if(response.ok)
                        {
                            Swal.fire(
                            'Your post has been removed.',
                            '',
                            'success'
                            )
                            .then(response => {
                                if(response.value)
                                    window.location.reload();
                            })
                        }
                        else
                        {
                            Swal.fire(
                            'There\'s an error removing your post.',
                            '',
                            'error'
                            )
                            .then(response => {
                                if(response.value)
                                    window.location.reload();
                            })
                        }
                    })
                }
            })
        }

        function socialShare(e)
    {
      console.log(e);
      var post = e.target.attributes.post.value;
      var id = e.target.attributes.id.value;
      var url = e.target.attributes.url.value;
      let twurl = "{{'https://twitter.com/intent/tweet?url='.urlencode(url().'/post/')}}"+post+"/via@IdeaSpies";
      let inurl = "{{'https://www.linkedin.com/sharing/share-offsite/?url='.urlencode(url().'/post/')}}"+post;

      if(id=='fb')
        {
        FB.ui({
            method: 'share',
            href: url,
        }, function(response){});
          submitPost(1,post);
        }
      else if(id=='tw')
        {
          submitPost(2,post);
          window.open(twurl);
        }
      else if (id=='in')
        {
          submitPost(3,post);
          window.open(inurl);
        }
      else
        {
          submitPost(4,post);
        }
      
      function submitPost(type,post){
          var  url = '{{url()}}/share/'+post+'/'+type;
          $.get(url);
      }
    };
    </script>
@stop