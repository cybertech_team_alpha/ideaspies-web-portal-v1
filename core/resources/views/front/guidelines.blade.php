@extends('layouts.front.master')
@section('title','Guidelines to Post')

@section('css')
  <style media="screen">
  #shortIntroVideo{
    width: 500px;
  }
  .background-1{
    background: #eeeeee; /* Old browsers */
    background: -moz-linear-gradient(top, #eeeeee 0%, #efefef 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, #eeeeee 0%,#efefef 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, #eeeeee 0%,#efefef 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#efefef',GradientType=0 ); /* IE6-9 */
  }
  @media (max-width: 575px) {
    #shortIntroVideo{
      width: 100% !important;
    }
  }

  .team-boxed {
    color:#313437;
  }

  .team-boxed p {
    color:#7d8285;
  }

  .team-boxed h2 {
    font-weight:bold;
    margin-bottom:40px;
    padding-top:40px;
    color:inherit;
  }

  @media (max-width:767px) {
    .team-boxed h2 {
      margin-bottom:25px;
      padding-top:25px;
      font-size:24px;
    }
  }

  .team-boxed .intro {
    font-size:16px;
    max-width:500px;
    margin:0 auto;
  }

  .team-boxed .intro p {
    margin-bottom:0;
  }


  .team-boxed .item {
    text-align:center;
  }

  .team-boxed .item .box {
    text-align:center;
    padding:15px;
    background-color:rgba(11, 20, 51, 0.1);
    margin-bottom:15px;
  }

  .team-boxed .item .name {
    font-weight:bold;
    margin-top:28px;
    margin-bottom:8px;
    color:inherit;
  }

  .team-boxed .item .title {
    text-transform:uppercase;
    font-weight:bold;
    color:#d0d0d0;
    letter-spacing:2px;
    font-size:13px;
  }

  .team-boxed .item .description {
    font-size:15px;
    margin-top:15px;
    margin-bottom:20px;
  }

  .team-boxed .item img {
    max-width:160px;
  }

  .team-boxed .social {
    font-size:18px;
    color:#a2a8ae;
  }

  .team-boxed .social a {
    color:inherit;
    margin:0 10px;
    display:inline-block;
    opacity:0.7;
  }

  .team-boxed .social a:hover {
    opacity:1;
  }

  .support-horizontal {
    color:#313437;
    /* background-color:#fff; */
  }

  .support-horizontal p {
    color:#7d8285;
  }

  .support-horizontal h2 {
    font-weight:bold;
    margin-bottom:40px;
    padding-top:40px;
    color:inherit;
  }

  @media (max-width:767px) {
    .support-horizontal h2 {
      margin-bottom:25px;
      padding-top:25px;
      font-size:24px;
    }
  }

  .support-horizontal .intro {
    font-size:16px;
    max-width:500px;
    margin:0 auto 10px;
  }



  .support-horizontal .item {
    padding-bottom:40px;
    min-height:160px;
  }

  @media (max-width:767px) {
    .support-horizontal .item {
      padding-bottom:30px;
      min-height:160px;
    }
  }

  .support-horizontal .item .name {
    font-size:18px;
    font-weight:bold;
    margin-top:10px;
    margin-bottom:15px;
    color:inherit;
  }

  @media (max-width:991px) {
    .support-horizontal .item .name {
      margin-top:22px;
    }
  }

  .support-horizontal .item .description {
    font-size:15px;
    margin-bottom:0;
  }

  h6 {
    font-size: 1.25rem !important;
  }


  </style>
@endsection

@section('content')

  <div class="container pb-4">
    <div class="">
      <h3 class="text-center">Guidelines To Post</h3>
      <span class="pageTitleUnderline mb-4"></span>

        <div class="row">
          <div class="col-lg-6">
            <iframe width="100%" height="250" src="https://www.youtube.com/embed/Cw16fJKbMmU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          <div class="col-lg-6">
            <p>
              IdeaSpies is encouraging you to be observant and post clever ideas you see that improve our lives. Our purpose is to inspire action by sharing innovation.
            </p>
            <p>
              These guidelines are designed to help you post a clever idea on IdeaSpies that is explained simply so it will be sharedand/or to submit an article related to innovation.</p>
              <p> When you join IdeaSpies you can also receive IdeaSpies Weekly.</p>  
              <p>This is a free service.</p>
          </div>

          <div class="col-lg-12">
            
               <h6><strong>Posting an idea</strong></h6>
            
            <p>
              IdeaSpies showcases ideas and innovations. All ideas on IdeaSpies are explained simply so they  can be understood easily.
            </p>
            <p>"If you can't explain it simply, you don't understand it well enough." Einstein</p>
            <p>More than 2,000 curated ideas and innovations can be searched easily by category or word.</p>
            <p>
              When you see or have an idea you believe is clever:
            </p>
            <ol>
              <li>Join <a href="{{url('register')}}">IdeaSpies</a></li>
              <li>Tap the “Post Idea” button </li>
            </ol>
          </div>
        </div>
        <hr>
        <div class="row">
          <h6><strong>Posting with an image</strong></h6>
          <div class="col-sm-12">
            <ul class="pl-3">
              <li>
                <strong>Idea Title</strong> - Choose a positive title that conveys <strong>a benefit</strong> or something different, not just the product name
              </li>
              <li>
                <strong>Category</strong> - Choose the idea category from the drop down menu
              </li>
              <li>
                <strong>Description</strong> - Describe the <strong>essence of the idea</strong> so people can understand it quickly. If it’s a startup state the “elevator pitch”. Write max 100 words . You can add a link for more information. You’re also welcome to include your email address if you want people to contact you.
              </li>
              <li>
                <strong>Add an image</strong> - Choose an image taken by your phone or from your photo library that shows the product or service well in <strong>horizontal (landscape)</strong> form. The image should be between 300 pixels and 1000 pixels height and width and below 10Mb -not a logo. If you need an image try <a href="https://pixabay.com/">Pixabay</a> for free ones you can use.
              </li>
            </ul>
          </div>
        </div>
        <hr>

        <div class="row">
          <h6><strong>Posting with a Video</strong></h6>
          <div class="col-sm-12">
            <ul class="pl-3">
              <li>
                <strong>Idea Title</strong> - Choose a positive title that conveys <strong>a benefit</strong> or something different, not just the product name
              </li>
              <li>
                <strong>Category</strong> - Choose the idea category from the drop down menu
              </li>
              <li>
                <strong>Description</strong> - Describe the <strong>essence of the idea</strong> so people can understand it quickly. If it’s a startup state the “elevator pitch”. Write max 100 words . You can add a link for more information. You’re also welcome to include your email address if you want people to contact you.
              </li>
              <li>
                <strong>Add a video</strong> - Choose a video taken by your phone or from your video library that shows the product or service well in horizontal (landscape) form. The video should be below 3 minutes length and below 10Mb.
              </li>
            </ul>
          </div>
        </div>
        <hr>


        <div class="row">
          <h6><strong>Posting with a URL</strong></h6>
          <div class="col-sm-12">
            <ul class="pl-3">
              <li>Add an idea title and category as above.</li>
              <li>Copy and paste only your idea URL (website address), starting with HTTP, with no space before or after.
              </li>
              <li>Check the preview before submitting</li>
            </ul>
          </div>
        </div>
        <hr>
        <div class="row">
          <h6><strong>Editing and deleting your post</strong></h6>
          <div class="col-sm-12">
            <p>
              By going to “My Posts” in the menu bar you can edit and delete your posts. After your idea has been shared changes can’t be made to the headline or photo.
            </p>
          </div>
        </div>
          <hr>
        <div class="row">
          <h6><strong>Sharing your idea</strong></h6>
          <div class="col-sm-12">
            <p>
              Rate your idea using the 5 stars then share it to your social media with the share icons to maximise awareness.When you share your idea add @IdeaSpies so we can like or comment.
            </p>
          </div>
        </div>
          <hr>
        <div class="row">
          <h6><strong>Questions/Comments</strong></h6>
          <div class="col-sm-12">
            <p>
              If these guidelines aren’t followed your post may be deleted by the administrator.
If you have any questions or comments on these guidelines please contact <a href="https://www.ideaspies.com/lynn.wood@ideaSpies.com">lynn.wood@ideaSpies.com</a>
            </p>
          </div>
        </div>
        <hr>
          
        <div class="row">
          <h6><strong>Congratulations!</strong></h6>
          <div class="col-sm-12">
            <p>
              After posting an idea you’re an Idea Spy! 
            </p>
            <p>
              Idea Spies are observant people who care about the world and make it better by sharing clever ideas happening around them  
            </p>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <p>
              You’re welcome to join the IdeaSpies LinkedIn Group <a href="https://www.linkedin.com/groups/6969692" target="_blank">here</a>
            </p>
            <p>
              You can also buy an IdeaSpies T Shirt <a href="https://www.spreadshirt.com.au/ideaspies" target="_blank">here</a>
            </p>
            <p>
              We’re creating a community of Idea Spies around the world who are truly embracing innovation.
            </p>
          </div>
        </div>
        <hr>

        <div class="row">
          <h6><strong>Submitting an article</strong></h6>
          <div class="col-sm-12">
            <p>After you <a href="http://test.ideaspies.com/register">join</a> IdeaSpies you are welcome to submit an article for publication on the IdeaSpies <a href="http://test.ideaspies.com/blog">Innovation Blog</a>.
            </p>
      
      <p>Your article needs to be:</p>
      <ol>
        <li>related to innovation</li>
        <li>in good English</li>
        <li>interesting</li>
        <li>authored by someone who has a public profile</li>
        <li>associated with a reputable organisation if an organisation’s name is to be included</li>
        <li>in word format</li>
        <li>with a suitable landscape image attached separately</li>
      </ol>
      <p>Articles need to be sent to us to be considered. If an article is accepted it will be edited by IdeaSpies to ensure it is compatible with other articles on the platform before publication.</p>
      <p>Lynn Wood</p>
      <a href="mailto:lynn.wood@IdeaSpies.com">lynn.wood@IdeaSpies.com</a>
            </p>
          </div>
        </div>

    </div>
  </div>

@endsection
