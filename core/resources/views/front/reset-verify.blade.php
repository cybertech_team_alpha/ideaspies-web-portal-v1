@extends('layouts.front.master') @section('title','Login | www.cybertech.com')

@section('css')
  <style media="screen">
  .socialSignIn .fab {
    padding: 7px;
    font-size: 30px;
    width: 30px;
    text-align: center;
    text-decoration: none;
    margin: 5px 2px;
    border-radius: 50%;
    font-size: 15px;
  }

  .socialSignIn .fab:hover {
    opacity: 0.7;
    color: #fff;
    text-decoration: none;
  }
  .socialSignIn .fa-facebook-f {
    background: #3B5998;
    color: white;
  }
  .socialSignIn .fa-google {
    background: #dd4b39;
    color: white;
  }
  .socialSignIn .fa-linkedin-in{
    background: #007bb5;
    color: white;
  }
  .socialSignIn .fa-twitter {
    background: #55ACEE;
    color: white;
  }
  </style>
@endsection
@section('content')
  <div class="container">
    <div class="login-card">
      {{--  <img src="{{ asset('assets/front/images/avatar_2x.png') }}" class="profile-img-card">  --}}


      <form class="form-horizontal" method="POST" onsubmit="validatePassword(event)" action="{{url('processReset/'.Session::get('userId'))}}">
        {{ csrf_field() }}

          <div class="">
            @if(session('message'))
              <strong>{{ session('message')}}</strong>
              <br>
              <br>
            @endif
            <p><div id="error-nwl"></div></p>
            <input id="password" type="password" placeholder="New Password" class="form-control" name="password" value="{{ old('password') }}" required autofocus onkeyup="checkPass(); return false;">
            <br>
            <input id="confirm" type="password" placeholder="Confirm New Password" class="form-control" name="confirm" value="{{ old('confirm') }}" required autofocus onkeyup="checkPass(); return false;" >
            <br>
            <input id="token" type="hidden"  name="token" value="{{Session::get('token') }}" required autofocus>
            <br>
          </div>




        <button class="btn btn-primary btn-block btn-lg btn-signin" type="submit">Change Password</button>
      </form>

    </div>
  </div>
@endsection
@section('js')
  <script>
    function validatePassword(e)
    {
      e.preventDefault();
      if(e.target[1].value == e.target[2].value)
        e.target.submit();
      else
        alert('Passwords do not match');
    }
  </script>
  <script type="text/javascript">
     function checkPass()
         {
         var password = document.getElementById('password');
         var confirm = document.getElementById('confirm');
         var message = document.getElementById('error-nwl');
         var goodColor = "#66cc66";
         var badColor = "#ff6666";

         if(password.value.length > 5)
         {
             password.style.backgroundColor = goodColor;
             message.style.color = goodColor;
             message.innerHTML = "character number ok!"
         }
         else
         {
             password.style.backgroundColor = badColor;
             message.style.color = badColor;
             message.innerHTML = " you have to enter at least 6 digit!"
             return;
         }

         if(password.value == confirm.value)
         {
             confirm.style.backgroundColor = goodColor;
             message.style.color = goodColor;
             message.innerHTML = "ok!"
         }
         else
         {
             confirm.style.backgroundColor = badColor;
             message.style.color = badColor;

         message.innerHTML = " These passwords don't match"
         }
         }
  </script>
@endsection
