@extends('layouts.front.master') @section('title','Welcome | www.cybertech.com')

<!-- CSS FOR THIS PAGE -->
@section('css')

<style type="text/css">
a:hover{
  text-decoration: none;
  color: #000;
}
.post-wrapper {
  /*height: 238px;*/
  margin-bottom: 40px;
}
.post-title {
  font-weight: 600;
  color: #000;
  display: block; /* or inline-block */
  text-overflow: ellipsis;
  word-wrap: break-word;
  overflow: hidden;
  max-height: 2.6em;
  line-height: 1.3em;
  height: 2.6em;
}
.description {
  display: block!important;
  text-overflow: ellipsis!important;
  word-wrap: break-word!important;
  overflow: hidden!important;
  max-height: 3.9em!important;
  min-height: 3.9em!important;
  line-height: 1.3em!important;
  height: 3.9em!important;
  margin: 5px auto;
}
.read-more {
  text-align: right;
}
.myP h1 {
  font-weight: 600!important;
  color: #000!important;
  display: block!important;
  text-overflow: ellipsis!important;
  word-wrap: break-word!important;
  overflow: hidden!important;
  max-height: 2.6em!important;
  min-height: 2.6em!important;
  line-height: 1.3em!important;
  height: 2.6em!important;
  font-size: 1rem!important;
  margin-top: 33px!important;
}
.guteurlsImg201610 {
  position: relative!important;
  padding-bottom: 56.25%!important;
  padding-top: 30px!important;
  height: 0!important;
  overflow: hidden!important;
}
.guteurlsTop, .guteurlsBottom {
  display: none!important;
}
.post-title:hover {
  color: #d64c1e;
}
.title-1 {
  color: #126dd0e6;
}
.myP p {
  text-overflow: ellipsis!important;
  overflow: hidden!important;
  line-height: 1.3em!important;
  min-height: 3.9em!important;
  max-height: 3.9em!important;
  overflow-wrap: break-word!important;
  word-wrap: break-word!important;
  font-family: 'pt-sans'!important;
  font-size: 14px!important;
  font-weight: 400!important;
}

.row.footerLogoBar {
  /*margin-top: 250px;*/
}
@media only screen and (min-width: 768px) and (max-width: 1199px) {
  div.guteurlsVideo {
    min-height: 220px;
  }
  .margin-after-url-desc {
    min-height: 150px;
  }
}

@media only screen and (min-width: 1200px) {
  .margin-after-url-desc {
    min-height: 150px;
  }
}
</style>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">



@stop
<!-- BODY -->

@section('content')

  <div class="container pb-4">

    <div class="raw text-right pb-2">
      <div class="filterList">
      <a href="?type=latest" class="font-weight-bold">Latest</a> |
        <a href="?type=popular" class="font-weight-bold">Popular</a>
      </div>
    </div>
    <script async src="https://guteurls.de/guteurls.js" email="rrdvd6enet42xecit" selector=".myP"></script>
    <div class="infinite-scroll">
        <div class="row ">
            {{--  idea of the week goes here  --}}
            @if(Request::is('search'))
                @if(empty(sizeof($posts)))
                    <div class="col-md-6 col-sm-12 col-xs-12 mx-auto text-center">
                        <h2>Search Results for "{{Request::input('q')}}" :</h2>
                        <h5>Sorry, no posts found</h5>
                    </div>
                @endif
            @endif
            @foreach($posts as $post)
                @if ($post->type == 'url')

                  <div class="col-md-4 post-wrapper">
                      <div class="img-container post-container">

                          <input type="hidden" class="ideaw" value="{{$post->idea_of_the_week ?? ''}}">
                          <a href="{{url('post/'.$post->id)}}">
                          <div class="myP">
                            <p style="display:none;">{{$post->url}}</p>
                          </div>
                          </a>
                      </div>
                      <div>
                          <!-- <p class="py-2 mb-0 title-p">
                              @if($post->idea_of_the_week == 1)
                                  <span class="title-red">Trending</span>
                              @endif
                              <span class="title-1 pull-right">{{$post->category->name ?? ''}}</span>

                          </p>
                          <a href="{{url('post/'.$post->id)}}">
                              <h6 class="title-2 post-title">{{$post->title}}</h6>
                          </a>
                          <div class="description">
                              {{substr(strip_tags($post->content),0,130)}}
                              <a href="{{url('post/'.$post->id)}}">
                                  <span class="btn-readMore">Read More &#10148;</span>
                              </a>
                          </div>

                          <div class="hashTags">
                              @foreach(array_slice(json_decode(json_encode($post->tagged)),0,3) as $tag)
                                  <span>#{{$tag->tag_slug}} </span>
                              @endforeach
                          </div> -->
                          <div class="margin-after-url-desc">
                            &nbsp;
                          </div>
                          <div class="read-more">
                            <a href="{{url('post/'.$post->id)}}">
                                <span class="btn-readMore">Read More &#10148;</span>
                            </a>
                          </div>
                          <div class="dateAndName">
                              <span>{{date("jS F Y", strtotime($post->created_at))}}</span>
                              <span>by {{$post->addedUser->first_name.' '.$post->addedUser->last_name}}</span>
                              <a href="{{url('post/'.$post->id).'?comment=1'}}">
                                  <span> • Comments</span>
                              </a>
                          </div>
                          @if (\Request::is('/'))
                          <div class="">
                              {{--<div class='starrr' id='{{$post->id}}'></div>--}}
                              <div style="display: inline-block;" class="rateYo" id="{{$post->id}}" data-rateyo-rating="{{ $post->ratings->avg('value') ?? 0}}"></div>
                              <span>({{ $post->ratings->count('value') ?? 0}} vote(s), average {{ round($post->ratings->avg('value'),1) ?? 0}} out of 5)</span>
                          </div>
                          @endif

                          <div>
                              <ul class="socialShere">
                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-fb" type="button" name="button" id='fb' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                              <i class="fab fa-facebook-f"></i>   Share  {{--   {{$post->share->where('type',1)->isEmpty()?'':$post->share->where('type',1)[0]->value}} --}}
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-tw" type="button"  name="button" id='tw' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}" title="{{$post->title}}">
                                              <i class="fab fa-twitter"></i>  Tweet
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-in" type="button" name="button" id='in' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                              <i class="fab fa-linkedin-in"></i> Share
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)" href="{{'mailto:?subject=Ideaspies : '.$post->title.'&body=Idea Link : '.url('post/'.$post->id)}}" class="btn btn-share btnShareEmail"
                                         title="Share by Email" id="email" post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                          <i class="fa fa-envelope" aria-hidden="true"></i> Email
                                      </a>
                                  </li>

                                  @if(Request::is('my-posts'))
                                  <li>
                                      <a href="{{url('admin/post/edit/'.$post->id)}}">
                                          <i class="fa fa-edit fa-lg" style="margin-left:10px;"></i>
                                      </a>

                                  </li>
                                  <li>
                                      <a href="" onclick="deletePost('{{$post->id}}',event)">
                                          <i class="fa fa-trash fa-lg" style="margin-left:10px;"></i>
                                      </a>
                                  </li>
                                  @endif
                              </ul>
                          </div>
                      </div>
                  </div>

                @elseif ($post->type == 'video')

                  <div class="col-md-4 post-wrapper">
                      <div class="img-container post-container">

                          <input type="hidden" class="ideaw" value="{{$post->idea_of_the_week ?? ''}}">
                          <a href="{{url('post/'.$post->id)}}">
                              <video src="{{ asset($post->video_path) }}" controls="true" height="238px"></video>
                          </a>
                      </div>
                      <div>
                          <p class="py-2 mb-0 title-p">
                              @if($post->idea_of_the_week == 1)
                                  <span class="title-red">Trending</span>
                              @endif
                              <span class="title-1 pull-right">{{$post->category->name ?? ''}}</span>

                          </p>
                          <a href="{{url('post/'.$post->id)}}">
                              <h6 class="title-2 post-title">{{$post->title}}</h6>
                          </a>
                          <div class="description">
                              <?php
                              //{!! substr(strip_tags($post->content,"<a>"), 0, 130) !!}
                              $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
                              $post->content = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $post->content);
                              echo substr(strip_tags($post->content,"<a>"), 0, 130);
                              ?>
                          </div>
                          <div class="read-more">
                            <a href="{{url('post/'.$post->id)}}">
                                <span class="btn-readMore">Read More &#10148;</span>
                            </a>
                          </div>
                          <!-- <div class="hashTags">
                              @foreach(array_slice(json_decode(json_encode($post->tagged)),0,3) as $tag)
                                  <span>#{{$tag->tag_slug}} </span>
                              @endforeach
                          </div> -->
                          <div class="dateAndName">
                              <span>{{date("jS F Y", strtotime($post->created_at))}}</span>
                              <span>by {{$post->addedUser->first_name.' '.$post->addedUser->last_name}}</span>
                              <a href="{{url('post/'.$post->id).'?comment=1'}}">
                                  <span> • Comments</span>
                              </a>
                          </div>
                          @if (\Request::is('/'))
                          <div class="">
                              {{--<div class='starrr' id='{{$post->id}}'></div>--}}
                              <div style="display: inline-block;" class="rateYo" id="{{$post->id}}" data-rateyo-rating="{{ $post->ratings->avg('value') ?? 0}}"></div>
                              <span>({{ $post->ratings->count('value') ?? 0}} vote(s), average {{ round($post->ratings->avg('value'),1) ?? 0}} out of 5)</span>
                          </div>
                          @endif

                          <div>
                              <ul class="socialShere">
                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-fb" type="button" name="button" id='fb' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                              <i class="fab fa-facebook-f"></i>   Share  {{--   {{$post->share->where('type',1)->isEmpty()?'':$post->share->where('type',1)[0]->value}} --}}
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-tw" type="button" name="button" id='tw' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}" title="{{$post->title}}">
                                              <i class="fab fa-twitter"></i>  Tweet
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-in" type="button" name="button" id='in' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                              <i class="fab fa-linkedin-in"></i> Share
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)" href="{{'mailto:?subject=Ideaspies : '.$post->title.'&body=Idea Link : '.url('post/'.$post->id)}}" class="btn btn-share btnShareEmail"
                                         title="Share by Email" id="email" post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                          <i class="fa fa-envelope" aria-hidden="true"></i> Email
                                      </a>
                                  </li>

                                  @if(Request::is('my-posts'))
                                  <li>
                                      <a href="{{url('admin/post/edit/'.$post->id)}}">
                                          <i class="fa fa-edit fa-lg" style="margin-left:10px;"></i>
                                      </a>

                                  </li>
                                  <li>
                                      <a href="" onclick="deletePost('{{$post->id}}',event)">
                                          <i class="fa fa-trash fa-lg" style="margin-left:10px;"></i>
                                      </a>
                                  </li>
                                  @endif
                              </ul>
                          </div>
                      </div>
                  </div>

                @else

                  <div class="col-md-4 post-wrapper">
                      <div class="img-container post-container">

                          <input type="hidden" class="ideaw" value="{{$post->idea_of_the_week ?? ''}}">
                          <a href="{{url('post/'.$post->id)}}">
                              <img src="{{ $post->img_path ? asset((file_exists($post->img_path) ? $post->img_path : 'assets/front/images/no_image.png') ): asset('/assets/front/images/no_image.png') }}" alt="post-image" width="100%">
                          </a>
                      </div>
                      <div>
                          <p class="py-2 mb-0 title-p">
                              @if($post->idea_of_the_week == 1)
                                  <span class="title-red">Trending</span>
                              @endif
                              <span class="title-1 pull-right">{{$post->category->name ?? ''}}</span>

                          </p>
                          <a href="{{url('post/'.$post->id)}}">
                              <h6 class="title-2 post-title">{{$post->title}}</h6>
                          </a>
                          <div class="description">
                              <?php
                              //{!! substr(strip_tags($post->content,"<a>"), 0, 130) !!}
                              $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
                              $post->content = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $post->content);
                              echo substr(strip_tags($post->content,"<a>"), 0, 130);
                              ?>
                          </div>
                          <div class="read-more">
                            <a href="{{url('post/'.$post->id)}}">
                                <span class="btn-readMore">Read More &#10148;</span>
                            </a>
                          </div>
                          <!-- <div class="hashTags">
                              @foreach(array_slice(json_decode(json_encode($post->tagged)),0,3) as $tag)
                                  <span>#{{$tag->tag_slug}} </span>
                              @endforeach
                          </div> -->
                          <div class="dateAndName">
                              <span>{{date("jS F Y", strtotime($post->created_at))}}</span>
                              <span>by {{$post->addedUser->first_name.' '.$post->addedUser->last_name}}</span>
                              <a href="{{url('post/'.$post->id).'?comment=1'}}">
                                  <span> • Comments</span>
                              </a>
                          </div>

                          @if (\Request::is('/'))
                          <div class="">
                              {{--<div class='starrr' id='{{$post->id}}'></div>--}}
                              <div style="display: inline-block;" class="rateYo" id="{{$post->id}}" data-rateyo-rating="{{ $post->ratings->avg('value') ?? 0}}"></div>
                              <span>({{ $post->ratings->count('value') ?? 0}} vote(s), average {{ round($post->ratings->avg('value'),1) ?? 0}} out of 5)</span>
                          </div>
                          @endif

                          <div>
                              <ul class="socialShere">
                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-fb" type="button" name="button" id='fb' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                              <i class="fab fa-facebook-f"></i>   Share  {{--   {{$post->share->where('type',1)->isEmpty()?'':$post->share->where('type',1)[0]->value}} --}}
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-tw" type="button" name="button" id='tw' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}" title="{{$post->title}}">
                                              <i class="fab fa-twitter"></i>  Tweet
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)">
                                          <button class="btn btn-share btn-in" type="button" name="button" id='in' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                              <i class="fab fa-linkedin-in"></i> Share
                                          </button>
                                      </a>
                                  </li>

                                  <li>
                                      <a onclick="socialShare(event)" href="{{'mailto:?subject=Ideaspies : '.$post->title.'&body=Idea Link : '.url('post/'.$post->id)}}" class="btn btn-share btnShareEmail"
                                         title="Share by Email" id="email" post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                          <i class="fa fa-envelope" aria-hidden="true"></i> Email
                                      </a>
                                  </li>

                                  @if(Request::is('my-posts'))
                                  <li>
                                      <a href="{{url('admin/post/edit/'.$post->id)}}">
                                          <i class="fa fa-edit fa-lg" style="margin-left:10px;"></i>
                                      </a>

                                  </li>
                                  <li>
                                      <a href="" onclick="deletePost('{{$post->id}}',event)">
                                          <i class="fa fa-trash fa-lg" style="margin-left:10px;"></i>
                                      </a>
                                  </li>
                                  @endif
                              </ul>
                          </div>
                      </div>
                  </div>

                @endif




                <!-- <div class="col-md-4 post-wrapper">
                    <div class="img-container post-container">

                        <input type="hidden" class="ideaw" value="{{$post->idea_of_the_week ?? ''}}">
                        <a href="{{url('post/'.$post->id)}}">
                        @if ($post->type == 'url')
                        <div class="myP">
                          <p style="display:none;">{{$post->url}}</p>
                        </div>
                        @elseif ($post->type == 'video')
                            <video src="{{ asset($post->video_path) }}" controls="true" height="238px"></video>
                        @else
                            <img src="{{ $post->img_path ? asset((file_exists($post->img_path) ? $post->img_path : 'assets/front/images/no_image.png') ): asset('/assets/front/images/no_image.png') }}" alt="post-image" width="100%">
                        @endif
                        </a>
                    </div>
                    <div>
                        <p class="py-2 mb-0 title-p">
                            @if($post->idea_of_the_week == 1)
                                <span class="title-red">Trending</span>
                            @endif
                            <span class="title-1 pull-right">{{$post->category->name ?? ''}}</span>

                        </p>
                        <a href="{{url('post/'.$post->id)}}">
                            <h6 class="title-2 post-title">{{$post->title}}</h6>
                        </a>
                        <div class="description">
                            {{substr(strip_tags($post->content),0,130)}}
                            <a href="{{url('post/'.$post->id)}}">
                                <span class="btn-readMore">Read More &#10148;</span>
                            </a>
                        </div>
                        <div class="hashTags">
                            @foreach(array_slice(json_decode(json_encode($post->tagged)),0,3) as $tag)
                                <span>#{{$tag->tag_slug}} </span>
                            @endforeach
                        </div>
                        <div class="dateAndName">
                            <span>{{date("jS F Y", strtotime($post->created_at))}}</span>
                            <span>by {{$post->addedUser->first_name.' '.$post->addedUser->last_name}}</span>
                            <a href="{{url('post/'.$post->id).'?comment=1'}}">
                                <span> • Comments</span>
                            </a>
                        </div>

                        <div class="">
                            {{--<div class='starrr' id='{{$post->id}}'></div>--}}
                            <div style="display: inline-block;" class="rateYo" id="{{$post->id}}" data-rateyo-rating="{{ $post->ratings->avg('value') ?? 0}}"></div>
                            <span>({{ $post->ratings->count('value') ?? 0}} vote(s), average {{ round($post->ratings->avg('value'),1) ?? 0}} out of 5)</span>
                        </div>

                        <div>
                            <ul class="socialShere">
                                <li>
                                    <a onclick="socialShare(event)">
                                        <button class="btn btn-share btn-fb" type="button" name="button" id='fb' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                            <i class="fab fa-facebook-f"></i>   Share  {{--   {{$post->share->where('type',1)->isEmpty()?'':$post->share->where('type',1)[0]->value}} --}}
                                        </button>
                                    </a>
                                </li>

                                <li>
                                    <a onclick="socialShare(event)">
                                        <button class="btn btn-share btn-tw" type="button" name="button" id='tw' post="{{$post->id}}" title="{{$post->title}}" url="{{Request::url().'/post/'.$post->id}}">
                                            <i class="fab fa-twitter"></i>  Tweet
                                        </button>
                                    </a>
                                </li>

                                <li>
                                    <a onclick="socialShare(event)">
                                        <button class="btn btn-share btn-in" type="button" name="button" id='in'  title="{{$post->title}}" url="{{Request::url().'/post/'.$post->id}}">
                                            <i class="fab fa-linkedin-in"></i> Share
                                        </button>
                                    </a>
                                </li>

                                <li>
                                    <a onclick="socialShare(event)" href="{{'mailto:?subject=Ideaspies : '.$post->title.'&body=Idea Link : '.url('post/'.$post->id)}}" class="btn btn-share btnShareEmail"
                                       title="Share by Email" id="email" post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                                        <i class="fa fa-envelope" aria-hidden="true"></i> Email
                                    </a>
                                </li>

                                @if(Request::is('my-posts'))
                                <li>
                                    <a href="{{url('admin/post/edit/'.$post->id)}}">
                                        <i class="fa fa-edit fa-lg" style="margin-left:10px;"></i>
                                    </a>

                                </li>
                                <li>
                                    <a href="" onclick="deletePost('{{$post->id}}',event)">
                                        <i class="fa fa-trash fa-lg" style="margin-left:10px;"></i>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div> -->
            @endforeach
            {!! $posts->render() !!}
        </div>

    </div>

  </div>

  <div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="regModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content" style="background-color: unset !important;">
              {{--<div class="modal-header">--}}
                  {{--<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>--}}
                  {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                      {{--<span aria-hidden="true">&times;</span>--}}
                  {{--</button>--}}
              {{--</div>--}}
              {{--<div class="SSmodal-body">--}}
                  <div class="login-card">
                      <div style="margin-top: -30px; margin-right: -20px;">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <br>
                      <strong>Get registered now & stay up to date with new Ideas
                      </strong>
                      <br>
                      <form class="form-horizontal" method="POST" action="{{ route('user.register') }}" enctype="multipart/form-data">
                          {{ csrf_field() }}

                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                              <label for="name" class=" control-label">Idea Spy Name*</label>

                              <div class="">
                                  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                  @if ($errors->has('name'))
                                      <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label for="email" class=" control-label">E-Mail Address*</label>

                              <div class="">
                                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="password" class=" control-label">Password*</label>

                              <div class="">
                                  <input id="password" type="password" class="form-control" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <label for="password-confirm" class=" control-label">Confirm Password*</label>

                              <div class="">
                                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                              </div>
                          </div>

                          <div class="form-group">
                              <label for="upload-image" class=" control-label"> Upload Image (optional) </label>
                              <div id="preview" class="preview"></div>
                              <div class="">
                                  <div class="file-upload">
                                      <input id="upload" class="file-upload__input" type="file" name="upload">
                                  </div>

                                  {{-- <input id="upload-image" class="file-upload__input" type="file" name="upload-image"> --}}
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="newsletter-div">
                                  <input id="newsletter-subscription" type="checkbox" name="subscription_status" checked>
                                  <span class="newsletter-span">Subscribe to IdeaSpies Weekly</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="">
                                  <button type="submit" class="btn btn-primary btn-block btn-lg btn-signin">
                                      Register
                                  </button>
                              </div>
                          </div>
                          <strong class="form-group">
                              <strong>
                                  Already a member? <a href="{{url('login')}}">Login</a>
                              </strong>
                          </div>
                      </form>
                  </div>

          </div>
      </div>

  </div>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
{{-- for linked in share btn  --}}
<script async src="https://static.addtoany.com/menu/page.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script src="//unpkg.com/jscroll/dist/jquery.jscroll.min.js"></script>
  <script type="text/javascript">
  $(function () {
    $(".rateYo").rateYo();
    $(".rateYo").rateYo("option", "starWidth", "13px");
    $(".rateYo").rateYo("option", "fullStar", true);
  });
  var token = "{{ csrf_token() }}";
  $(".rateYo").rateYo()
    .on("rateyo.set", function (e, data) {
        $.post('post-rate',{id : e.currentTarget.id, value : data.rating, _token : token})
    });
  @if(!Session::has('regModal') && Sentinel::check() == false)
      // window.setTimeout(function () {
      //     $('#regModal').modal()
      // },10000)
    {{Session::put('regModal',true)}}
  @endif
    $('ul.pagination').hide();
  $(function() {
      $('.infinite-scroll').jscroll({
          autoTrigger: true,
          loadingHtml: '<div class="row justify-content-center"><img class="center-block" src="{{url('assets/front/images/load.gif')}}" alt="Loading..." /></div>',
          padding: 0,
          nextSelector: '.pagination li.active + li a',
          contentSelector: 'div.infinite-scroll',
          callback: function() {
            document.guteUrls.execute();
              $('ul.pagination').remove();
              $(function () {
                    $(".rateYo").rateYo();
                    $(".rateYo").rateYo("option", "starWidth", "13px");
                    $(".rateYo").rateYo("option", "fullStar", true);
                });
                var token = "{{ csrf_token() }}";
                $(".rateYo").rateYo()
                    .on("rateyo.set", function (e, data) {
                        $.post('post-rate',{id : e.currentTarget.id, value : data.rating, _token : token})
                    });
          }
      });
  });
  </script>
    <script>
        function deletePost(post,e)
        {
            e.preventDefault();
            Swal.fire({
                title: 'Are you sure want to remove the post?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, remove it!'
            }).then((result) => {
                if (result.value) {
                    fetch("{{url('post/delete').'/'}}"+post)
                    .then(response => {
                        if(response.ok)
                        {
                            Swal.fire(
                            'Your post has been removed.',
                            '',
                            'success'
                            )
                            .then(response => {
                                if(response.value)
                                    window.location.reload();
                            })
                        }
                        else
                        {
                            Swal.fire(
                            'There\'s an error removing your post.',
                            '',
                            'error'
                            )
                            .then(response => {
                                if(response.value)
                                    window.location.reload();
                            })
                        }
                    })
                }
            })
        }

        function socialShare(e)
    {
      console.log(e);
      var post = e.target.attributes.post.value;
      var id = e.target.attributes.id.value;
      var url = e.target.attributes.url.value;
      let inurl = "{{'https://www.linkedin.com/sharing/share-offsite/?url='.urlencode(url().'/post/')}}"+post;

      if(id=='fb')
        {
        FB.ui({
            method: 'share',
            href: url,
        }, function(response){});
          submitPost(1,post);
        }
      else if(id=='tw')
        {
          var title = e.target.attributes.title.value;
          let twurl = "{{'https://twitter.com/intent/tweet?text='}}"+title+"{{'&url='.urlencode(url().'/post/')}}"+post+"&via=IdeaSpies";
          submitPost(2,post);
          window.open(twurl);
        }
      else if (id=='in')
        {
          submitPost(3,post);
          window.open(inurl);
        }
      else
        {
          submitPost(4,post);
        }

      function submitPost(type,post){
          var  url = '{{url()}}/share/'+post+'/'+type;
          $.get(url);
      }
    };
    </script>
@stop
