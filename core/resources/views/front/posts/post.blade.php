@extends('layouts.front.master') @section('title','Post | www.cybertech.com')

@section('css')

<meta name="twitter:description" content="{{substr($post->content,0,100)}}">
<meta name="twitter:creator" content="Ideaspies">
<meta name="twitter:site" content="@ideaspies">
<meta name="twitter:card" content="summary_large_image">

  <meta property="og:url" content="{{url().'/post/'.$post->id}}" />
  <meta property="og:type" content="article" />
  <meta property="og:title" content="Ideaspies - {{$post->title}}" />
  <meta property="og:description" content="{{substr($post->content,0,100)}}" />
  @if($post->type =='video')
    <meta property="og:video" content="{{filter_var($post->video_path, FILTER_VALIDATE_URL) ? $post->video_path : url().'/'.$post->video_path }}" />
  @else
    <meta name="twitter:image" content="{{filter_var($post->img_path, FILTER_VALIDATE_URL) ? $post->img_path : url().'/'.$post->img_path }}">      
    <meta property="og:image" content="{{filter_var($post->img_path, FILTER_VALIDATE_URL) ? $post->img_path : url().'/'.$post->img_path }}" />
  @endif

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

  <style media="screen">
  .post-details {
    background-color: #fff;
  }
  div.guteurlsTop {
    display: none!important;
  }
  div.guteurlsBox {
    max-width: 100% !important;
  }
  .post-details img, .post-details video {
    width:auto!important;
    height: auto;
    max-width: 100%;
    max-height: 400px;
    margin: 0 auto!important;
    display: block;
  }
  .postImg span{
    font-size: 11px !important;
    font-style: italic;
  }
  .postCatList{
    padding-left: 0 !important;
  }
  .postCatList li{
    display: inline-block;
    padding: 0px 6px;
  }
  .postCatList .selected{
    background: #d71933;
    color: #fff;
  }
  .postSocialLinks{
    width: 100%;
    text-align: center;
    display: block;
  }
  .btn-share {
    padding: 4px 10px;
    margin-top: 0px;
  }
  .postSocialLinks a{
    display: inline-block;
    padding: 0 5px;
    color: #333;
  }
  .postUser{
    max-width: 280px !important;
  }
  .a2a_default_style a{
    padding: 0 !important;
  }
  #rate:hover{
    cursor: pointer;
  }
  @media (max-width: 767px) {
    .postUser {
      margin-left: auto;
      margin-right: auto;
    }
  }
  </style>

@endsection

@section('content')


  <div class="container pb-4 text-center">
    <ul class="postCatList">
      <!-- <li class="selected">Exclusive</li>
      <li><a href="#">{{$post->category->name}}</a></li> -->

      @if($post->idea_of_the_week==1)
    <li class="selected">Trending</li>
    @endif
      <li>{{$post->category->name}}</li>
    </ul>

    <h3 class="">{{$post->title}}</h3>
    <span class="pageTitleUnderline mb-4"></span>

    <div class="row pt-3">

      <script async src="https://guteurls.de/guteurls.js" email="rrdvd6enet42xecit" selector=".myP"></script>

      <div class="col-sm-12 col-md-10 order-md-2">
        <div class="post-details">
          @if ($post->type == 'url')
          <div class="myP">
            <p style="display:none;">{{$post->url}}</p>
          </div>
          @elseif ($post->type == 'image')
          <img src="{{ $post->img_path ? asset((file_exists($post->img_path) ? $post->img_path : 'assets/front/images/no_image.png') ): asset('/assets/front/images/no_image.png') }}" alt="post-image" width="100%">
            <p class="text-justify mt-3">
              <?php
              //{!! substr(strip_tags($post->content,"<a>"), 0, 130) !!}
              $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
              $post->content = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $post->content);
              echo substr(strip_tags($post->content,"<a>"), 0, 130);
              ?>
            </p>
          @else
          <video src="{{ asset($post->video_path) }}" controls="true"></video>
            <p class="text-justify mt-3">
              <?php
              //{!! substr(strip_tags($post->content,"<a>"), 0, 130) !!}
              $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
              $post->content = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $post->content);
              echo substr(strip_tags($post->content,"<a>"), 0, 130);
              ?>
            </p>
          @endif
        </div>
        
         <!-- <div class="hashTags">
            @foreach(array_slice(json_decode(json_encode($post->tagged)),0,3) as $tag)
            <span>#{{$tag->tag_slug}} </span>
            @endforeach
          </div> -->

        <div>

            <ul class="socialShere">
                <li>
                    <a onclick="socialShare(event)">
                        <button class="btn btn-share btn-fb" type="button" name="button" id='fb' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                            <i class="fab fa-facebook-f"></i>   Share  {{--   {{$post->share->where('type',1)->isEmpty()?'':$post->share->where('type',1)[0]->value}} --}}
                        </button>
                    </a>
                </li>
                <li>
                    <a onclick="socialShare(event)">
                        <button class="btn btn-share btn-tw" type="button" name="button" id='tw' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                            <i class="fab fa-twitter"></i>  Tweet
                        </button>
                    </a>
                </li>
                <li>
                    <a onclick="socialShare(event)">
                        <button class="btn btn-share btn-in" type="button" name="button" id='in' post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                            <i class="fab fa-linkedin-in"></i> Share
                        </button>
                    </a>
                </li>
                <li>
                    <a onclick="socialShare(event)" href="{{'mailto:?subject=Ideaspies : '.$post->title.'&body=Idea Link : '.url('post/'.$post->id)}}" class="btn btn-share btnShareEmail"
                       title="Share by Email" id="email" post="{{$post->id}}" url="{{Request::url().'/post/'.$post->id}}">
                        <i class="fa fa-envelope" aria-hidden="true"></i> Email
                    </a>
                </li>
            </ul>

        </div>

        <h5>What do you think?</h5>

        @if(Sentinel::check())
        <a class="text-danger" href="{{url('user/logout')}}">Logged in as {{Sentinel::check()->first_name.' '.Sentinel::check()->last_name}}. Log out?</a>


        <p><strong>Comment</strong></p>
        <form id="commentForm" class="" action="{{url('post-comment')}}" method="post">
          {{csrf_field()}}
          <div style="text-align: left !important;">
            <input type="hidden" name="user_id" value="{{ Sentinel::check()? Sentinel::check()->id : null}}">
            <input type="hidden" name="post_id" value="{{$post->id}}">
            <textarea id="textarea" name="comment" rows="8" required ></textarea>
          </div>
          <div class="form-group">
            <button type="submit" id="post" class="btn btn-dark" onclick="validateComment()" >Post Comment</button>
            <button type="button" id="editBtn" style="display: none" class="btn btn-dark" id="editBtn'+id+'" onclick="javascript:submitEdit(($('#textarea').val()))" >Edit Comment</button>
          </div>
        </form> 
            @else
              <br>
              You must be
              <a href="{{url('login')}}">logged in</a>
               to post a comment.{{Session::put('loginRedirect', url('post').'/'.$post->id)}}
        @endif
        {{-- <hr> --}}
        <div>
          <h5>Comments</h5>
          @if($comments)
            @foreach ($comments as $comment)
              <div class=" mb-2">
                <div class="row col-6 pt-2">
                  <h5>{{$comment->user->first_name.' '.$comment->user->last_name}}</h5>
                </div>
                <div class="row col-12 pl-5" id="{{'comment'.$comment->id}}">
                  <?php echo $comment->comment;?>
                </div>
                
                @if(Sentinel::check() && $comment->user_id==Sentinel::check()->id)
                    <div class="col-12" id="commentActions">

                    </div>
                  <div class="row col-3" >
                    <div class="col-6">
                      <a href="{{url('comment/'.$comment->id.'/edit')}}" id="{{'edit'.$comment->id}}" comment-id="{{$comment->comment}}"  data-id="{{$comment->id}}" onclick="javascript:editComment(this);">Edit</a>
                    </div>
                    {{-- <button id="create">Create inputs</button> --}}
                    <div class="col-6">
                      <a href="{{url('comment/'.$comment->id.'/delete')}}" id="{{'delete'.$comment->id}}">Delete</a>
                    </div>
                  </div>
                @endif
              </div>
            @endforeach
          @endif
        </div>
      </div>


      <div class="col order-md-1 postUser">
        <div class="media" style="margin-left: auto;">
          <div class="media-body align-self-center">
             <img class="ml-10 profile" src="{{ asset(null!==($post->addedUser->img_path)? (file_exists($post->addedUser->img_path) ? $post->addedUser->img_path : 'assets/front/images/no_image.png') :'assets/front/images/temp/profilepic.png') }}" alt="Generic placeholder image">
    
            <p class="mb-1">By <strong style="color: #026CD0;">{{$post->addedUser->first_name.' '.$post->addedUser->last_name}}</strong></p>
            <p class="mb-1">{{date("jS F Y", strtotime($post->created_at))}}</p>
          </div>
        </div>
        <p class="text-center mb-0"><strong><span id="rate">Rate</span></strong></p>
        <div style="display: inline-block;" class="rateYo" id="{{$post->id}}" data-rateyo-rating="{{ $post->ratings->avg('value') ?? 0}}"> </div>
        <div>({{ $post->ratings->count('value') ?? 0}} vote(s), average {{ round($post->ratings->avg('value'),1) ?? 0}} out of 5)</div>

        <hr>
        {{--  <p class="text-center mb-0"><strong>Share</strong></p>  --}}
        <span class="postSocialLinks hide">
          <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
          <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
          <a target="_blank" href="#"><i class="fas fa-envelope"></i></a>
          <a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>
        </span>       
      </div>
    </div>

    <hr>
    <h4 class="text-center mb-4"><strong>Related Posts</strong></h4>
    <div class="">
      @include('front.components.related-posts')
    </div>

  </div>


<div class="modal fade" id="rateModal" tabindex="-1" role="dialog" aria-labelledby="rateModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Rate Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select class="form-control" id="rateValue" name="rate">
          <option value="" selected disabled>Please select</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="submitRate()">Submit</button>
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var editor=this;
var id=this;
  function editComment(item)
      { 
        event.preventDefault();
         id = $(item).attr('data-id');
          let comment = $('#'+id);
          let txt= $(item).attr('comment-id');
          
          if(localStorage.getItem('admin')){
          this.editor=new Jodit('#comment'+id);
          editor.value = comment.html();
          $('#'+item.id).hide();
          let btn = $('<br><input type="button" value="Edit Post" class="btn btn-dark" id="editBtn'+id+'" onclick="javascript:submitAdminEdit('+id+',($(\'#comment'+id+'\').html()))"/>');
          $('#delete'+id).hide().after(btn);
          }else{
            document.getElementById("textarea").value = txt;
            document.getElementById("editBtn").style.display='block';
            document.getElementById("post").style.display='none';
          }
          
          return id;
      }
      function submitEdit(val)
      {
          $.ajax({
              url: '{{url()}}/comment/'+this.id+'/edit',
              method: 'post',
              data: {
                  comment: val
              },
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              dataType: 'json',
              success: function (data) {
                  $('#editBtn'+id).remove();
                  $('#edit'+id).show();
                  $('#delete'+id).show()
                  $('#comment'+id).show()
              }
          });
          location.reload();    
      }

      function submitAdminEdit(id,val)
      {
          $.ajax({
              url: '{{url()}}/comment/'+this.id+'/edit',
              method: 'post',
              data: {
                  comment: val
              },
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              dataType: 'json',
              success: function (data) {
                  $('#editBtn'+id).remove();
                  $('#edit'+id).show();
                  $('#delete'+id).show()
                  $('#comment'+id).show()
              }
          });
          location.reload();  
          location.reload();   
      }
</script>
@endsection


