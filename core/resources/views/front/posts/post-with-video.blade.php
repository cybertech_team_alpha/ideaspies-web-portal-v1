@extends('layouts.front.master') @section('title','Register | www.cybertech.com')
@section('css')

  <style media="screen">
    .login-card{
      max-width: 500px;
      margin-top: 0;
    }
  </style>
  <link rel="stylesheet" href="{{asset('assets/back/styles/jquery.tag-editor.css')}}" />

@endsection

@section('content')

  <div class="container">

    <h3 class="text-center">Post Idea With Video</h3>
    <span class="pageTitleUnderline mb-4"></span>

    <div class="login-card">
      <p><a class="guidelinesLink" href="{{ url('guidelines') }}">See Guidelines for posting ideas</a></p>

      {{-- <form class="form-horizontal" method="POST" action="{{ route('register') }}"> --}}
      <form id="form" class="form-horizontal" method="POST" action="{{url('addPost')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="type" value="video">

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
          <label for="title" class=" control-label">Title*</label>

          <div class="">
            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>
            <label class="helpText" for="">A benefit, not the product name</label>
            @if ($errors->has('title'))
              <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
              </span>
            @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
          <label for="category" class=" control-label">Category*</label>

          <div class="">
            <select class="form-control" name="category_id" required autofocus>
              <option value="" disabled selected>Please Select</option>
              @foreach($data as $row)
              <option value="{{$row->id}}">{{$row->name}}</option>
              @endforeach
            </select>

            @if ($errors->has('category_id'))
              <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
              </span>
            @endif
          </div>
        </div>


        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
            <div class="text-left">
                Description
                </div>
                <div class="text-right">
                  <span id="left">100</span> words left</div>

          <div class="">
            <textarea id="description" name="content" class="form-control"></textarea>
            <label class="helpText" for="">Max 100 words plus link for more information</label>
            @if ($errors->has('content'))
              <span class="help-block">
                <strong>{{ $errors->first('content') }}</strong>
              </span>
            @endif
          </div>
        </div>


        <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
          <label for="video" class=" control-label">Video*</label>

          <div class="">
            <div id="upload-error"></div>
          <input type="file" class="form-control-file" name="video" id="video" required autofocus>
          <label class="helpText" for="">Min 5 seconds & 1Mb file<br/>Max 1 Minute & 30Mb file</label>
          <p id="timing" class="alert alert-danger" style="display:none">Please upload video less than one Mins length & 30Mb </p>
            @if ($errors->has('video'))
              <span class="help-block">
                <strong>{{ $errors->first('video') }}</strong>
              </span>
            @endif
          </div>
        </div>

      </form>

        <div class="form-group">
          <div class="{{ $errors->has('tags') ? 'has-error' : ''}}">
            {!! Form::label('tags', 'Tags ', ['class' => 'control-label']) !!}
            <div class="">
              {!! Form::text('tags',null, ['class' => 'form-control', 'placeholder' => '#hash #tags',  'data-role'=>"tagsinput"]) !!}
              {!! $errors->first('tags','<p class="help is-danger">:message</p>') !!}
            </div>
          </div>
        </div>



        <div class="form-group">
          <div class="">
            <button id="submitBtn" type="submit" class="btn btn-primary btn-block btn-lg btn-signin">
              Submit
            </button>
          </div>
        </div>
    </div>
  </div>

@endsection

@section('js')
  <script src="{{asset('assets/back/scripts/jquery.tag-editor.min.js')}}"></script>
  <script type="text/javascript">
    $('#tags').tagEditor()

    $("#description").on('keyup', function() {
        if (this.value.match(/\S+/g) == null)
        {
          var words = 0;
        }
        else
        {
          var words = this.value.match(/\S+/g).length;
        }
        if (words > 100) {
            var trimmed = $(this).val().split(/\s+/, 100).join(" ");
            $(this).val(trimmed + " ");
        }
        else if(words == 0) {
            $('#left').text('100');
        }
        else {
            $('#left').text(100-words);
        }
    });

    $('#video').bind('change', function(){
      var size = this.files[0].size/1024/1024;
      if($('#upload-error').length)
      {
        $('#upload-error').html('');
        $('#submitBtn').prop('disabled',false)
      }
      if(size > 30)
      {
        var styles = {
          color : 'red',
          fontWeight : 'bold'
        }
        $('#upload-error').html('File too Large');
        $('#upload-error').css(styles);
        $('#submitBtn').prop('disabled',true)
      }
    });

    $('#submitBtn').click(e => {

      let tags = $('#tags').tagEditor('getTags')[0].tags

      console.log(tags);


      tags.forEach((element,index) => {
        console.log('aa');

        $('#form').append('<input name="tags['+index+']" hidden value="'+element+'">');
      });

      $('#form').submit()
    })
  </script>

  <script type="text/javascript">
  var vid = document.createElement('video');
        document.querySelector('#video').addEventListener('change', function() {
        // create url to use as the src of the video
        var fileURL = URL.createObjectURL(this.files[0]);
        vid.src = fileURL;
        // wait for duration to change from NaN to the actual duration
        vid.ondurationchange = function() {
          //alert(this.duration);
          if (this.duration>60) {
            $("#submitBtn").hide();
            $("#timing").show();
          } else {
            $("#submitBtn").show();
            $("#timing").hide();
          }
        };
        });
  </script>
  <script type="text/javascript">
   $(document).ready(function(){
      $("#submitBtn").on('click',function(){
              $("#submitBtn").hide();
              $(document).ready(function() {
              $.blockUI({
                  message: '<h1><img src={{asset('assets/back/images/loading-bars.svg')}} /> Please wait!</h1>',
              })
          });
          });
      });
   </script>
@stop
