@extends('layouts.front.master') @section('title','Post | www.cybertech.com')

@section('css')

<meta name="twitter:description" content="{{substr($post->content,0,100)}}">
<meta name="twitter:creator" content="Ideaspies">
<meta name="twitter:site" content="@ideaspies">
<meta name="twitter:card" content="summary_large_image">

  <meta property="og:url"           content="{{url().'/post/'.$post->id}}" />
  <meta property="og:type"          content="article" />
  <meta property="og:title"         content="Ideaspies - {{$post->title}}" />
  <meta property="og:description"   content="{{substr($post->content,0,100)}}" />
  @if($post->type =='video')
    <meta property="og:video"         content="{{filter_var($post->video_path, FILTER_VALIDATE_URL) ? $post->video_path : url().'/'.$post->video_path }}" />
  @else
    <meta name="twitter:image" content="{{filter_var($post->img_path, FILTER_VALIDATE_URL) ? $post->img_path : url().'/'.$post->img_path }}">      
    <meta property="og:image"         content="{{filter_var($post->img_path, FILTER_VALIDATE_URL) ? $post->img_path : url().'/'.$post->img_path }}" />
  @endif

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

  <style media="screen">
  .post-details {
    background-color: #fff;
  }
  .post-details img, .post-details video {
    width:auto;
    height: auto;
    max-width: 100%;
    max-height: 400px;
  }
  
  .postImg span{
    font-size: 11px !important;
    font-style: italic;
  }
  .postCatList{
    padding-left: 0 !important;
  }
  .postCatList li{
    display: inline-block;
    padding: 0px 6px;
  }
  .postCatList .selected{
    background: #d71933;
    color: #fff;
  }
  .postSocialLinks{
    width: 100%;
    text-align: center;
    display: block;
  }
  .btn-share {
    padding: 4px 10px;
    margin-top: 0px;
  }
  .postSocialLinks a{
    display: inline-block;
    padding: 0 5px;
    color: #333;
  }
  .postUser{
    max-width: 280px !important;
  }

  .a2a_default_style a{
    padding: 0 !important;
  }
  #rate:hover{
    cursor: pointer;
  }
  @media (max-width: 767px) {
    .postUser {
      margin-left: auto;
      margin-right: auto;
    }
  }
  </style>

@endsection

@section('content')


  <div class="container pb-4 text-center">
    <ul class="postCatList">
      <!-- <li class="selected">Exclusive</li>
      <li><a href="#">{{$post->category->name}}</a></li> -->

      @if($post->idea_of_the_week==1)
    <li class="selected">Trending</li>
    @endif
      <li>{{$post->category->name}}</li>
    </ul>

    <h3 class="">{{$post->title}}</h3>
    <span class="pageTitleUnderline mb-4"></span>

    <div class="row pt-3">

      <script async src="https://guteurls.de/guteurls.js" email="rrdvd6enet42xecit" selector=".myP"></script>


      <div class="col-sm-12 col-md order-md-2 ">
        @if ($post->type == 'url')
          <div class="myP">
            <p style="display:none;">{{$post->url}}</p>
          </div>
        @else
        <div class="post-details">
<<<<<<< HEAD
      
          @if ($post->type == 'video')
            <video src="{{ asset($post->video_path) }}" controls="true"></video>
          @else
            <img src="{{ asset($post->img_path ? asset((file_exists($post->img_path) ? $post->img_path : 'assets/front/images/no_image.png') ): asset('/assets/front/images/no_image.png') ) }}" alt="post-image" width="100%">
          @endif

        </div>

        <p class="text-justify mt-3">
          <?php echo $post->content ?>
        </p>
=======
            @if ($post->type == 'image')
              <img src="{{ asset($post->img_path) }}" alt="post-image" width="100%">
            @endif
  
            @if ($post->type == 'video')
              <video src="{{ asset($post->video_path) }}" controls="true"></video>
            @endif
  
              
          </div>
          
      
          <p class="text-justify mt-3">
            <?php echo $post->content ?>
          </p>
        @endif
        
>>>>>>> e113ef317fa1fbf861503d2cf9074c93efe43e2d

        @if($post->type == 'url')
          <p style="text-align:center;">
            <a href="{{$post->url}}" target="_blank">Click here to see more.</a>
          </p>
        @endif
        
         <div class="hashTags">
            @foreach(array_slice(json_decode(json_encode($post->tagged)),0,3) as $tag)
            <span>#{{$tag->tag_slug}} </span>
            @endforeach
          </div>

        <div>

          <ul class="socialShere">
              <li>
                  {{--              <a href="http://www.facebook.com/sharer.php?u={{urlencode(Request::url().'/post/'.$post->id)}}" target="_blank">--}}
                  <a>
                      <button class="btn btn-share btn-fb" type="button" name="button" id='fb' data-post="{{$post}}" data-url="{{Request::url().'/post/'.$post->id}}">
                          <i class="fab fa-facebook-f fa-lg"></i> Share {{$post->share->where('type',1)->isEmpty()?'':$post->share->where('type',1)->pluck('value')[0]}}
                      </button>
                  </a>
              </li>

          <li>
              <button class="btn btn-share btn-tw" type="button" id="tw" name="button" data-post="{{$post}}" data-url="{{Request::url().'/post/'.$post->id}}">
                <i class="fab fa-twitter  fa-lg"></i>  Tweet {{$post->share->where('type',2)->isEmpty()?'':$post->share->where('type',2)->pluck('value')[0]}}
              </button>
          </li>

          <li>
              <button class="btn btn-share btn-in" type="button" id="in" name="button" data-post="{{$post}}" data-url="{{Request::url().'/post/'.$post->id}}">
                <i class="fab fa-linkedin-in fa-lg"></i>  Share {{$post->share->where('type',3)->isEmpty()?'':$post->share->where('type',3)->pluck('value')[0]}}
              </button>
          </li>
          <li>
            <a href="{{'mailto:?subject=Ideaspies : '.$post->title.'&body=Idea Link : '.url('post/'.$post->id)}}" class="btn btn-share btnShareEmail"
               title="Share by Email" data-post="{{$post}}" data-url="{{Request::url().'/post/'.$post->id}}">
                <i class="fa fa-envelope  fa-lg" aria-hidden="true"></i> Email {{$post->share->where('type',4)->isEmpty()?'':$post->share->where('type',4)->pluck('value')[0]}}
            </a>
          </li>
        </ul>

        </div>

        <h5>What do you think?</h5>

        @if(Sentinel::check())
        <a class="text-danger" href="{{url('user/logout')}}">Logged in as {{Sentinel::check()->first_name.' '.Sentinel::check()->last_name}}. Log out?</a>


        <p><strong>Comment</strong></p>
        <form id="commentForm" class="" action="{{url('post-comment')}}" method="post">
          {{csrf_field()}}
          <div style="text-align: left !important;">
            <input type="hidden" name="user_id" value="{{ Sentinel::check()? Sentinel::check()->id : null}}">
            <input type="hidden" name="post_id" value="{{$post->id}}">
            <textarea id="comment" name="comment" rows="8" cols="80" required ></textarea>
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-dark" onclick="validateComment()" me="button">Post Comment</button>
          </div>
        </form>
            @else
              <br>
              You must be
              <a href="{{url('login')}}">logged in</a>
               to post a comment.{{Session::put('loginRedirect', url('post').'/'.$post->id)}}
        @endif
        <hr>
        <div>
          <h5>Comments</h5>
          @if($comments)
            @foreach($comments as $comment)
              <div class=" mb-2">
                <div class="row col-6 pt-2">
                  <h5>{{$comment->user->first_name.' '.$comment->user->last_name}}</h5>
                </div>
                <div class="row col-12 pl-5" id="{{'comment'.$comment->id}}">
                  <?php echo $comment->comment; ?>
                </div>
                @if(Sentinel::check() && $comment->user_id==Sentinel::check()->id)
                    <div class="col-12" id="commentActions">

                    </div>
                  <div class="row col-3" >
                    <div class="col-6">
                      <a href="{{url('comment/'.$comment->id.'/edit')}}" id="{{'edit'.$comment->id}}" data-id="{{$comment->id}}" onclick="javascript:editComment(this)">Edit</a>
                    </div>
                    <div class="col-6">
                      <a href="{{url('comment/'.$comment->id.'/delete')}}" id="{{'delete'.$comment->id}}">Delete</a>
                    </div>
                  </div>
                @endif
              </div>
            @endforeach
          @endif
        </div>
      </div>


      <div class="col order-md-1 postUser">
        <div class="media" style="margin-left: auto;">
          <div class="media-body align-self-center">
            <p class="mb-1">By <strong style="color: #026CD0;">{{$post->addedUser->first_name.' '.$post->addedUser->last_name}}</strong></p>
            <p class="mb-1">{{date("jS F Y", strtotime($post->created_at))}}</p>
          </div>
          <img class="ml-3 profile" src="{{ asset(null!==($post->addedUser->img_path)? (file_exists($post->addedUser->img_path) ? $post->addedUser->img_path : 'assets/front/images/no_image.png') :'assets/front/images/temp/profilepic.png') }}" alt="Generic placeholder image">
        </div>
        <hr>
        {{--  <p class="text-center mb-0"><strong>Share</strong></p>  --}}
        <span class="postSocialLinks hide">
          <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
          <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
          <a target="_blank" href="#"><i class="fas fa-envelope"></i></a>
          <a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>
        </span>
        <p class="text-center mb-0"><strong><span id="rate">Rate</span></strong></p>

          <div style="display: inline-block;" class="rateYo" id="{{$post->id}}" data-rateyo-rating="{{ $post->ratings->avg('value') ?? 0}}">
          </div>
              <div>({{ $post->ratings->count('value') ?? 0}} vote(s), average {{ round($post->ratings->avg('value'),1) ?? 0}} out of 5)</div>
      </div>


    </div>

    <hr>
    <h4 class="text-center mb-4"><strong>Related Posts</strong></h4>
    <div class="">
      @include('front.components.related-posts')
    </div>

  </div>


<div class="modal fade" id="rateModal" tabindex="-1" role="dialog" aria-labelledby="rateModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Rate Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select class="form-control" id="rateValue" name="rate">
          <option value="" selected disabled>Please select</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="submitRate()">Submit</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
  {{--for linked in share btn--}}
  <script async src="https://static.addtoany.com/menu/page.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
  <script type="text/javascript">
  $(function () {
    $(".rateYo").rateYo();
    $(".rateYo").rateYo("option", "starWidth", "25px");
    $(".rateYo").rateYo("option", "fullStar", true);
  });
  var token = "{{ csrf_token() }}";
  $(".rateYo").rateYo()
    .on("rateyo.set", function (e, data) {
        $.post("{{url('post-rate')}}",{id : e.currentTarget.id, value : data.rating, _token : token})
    });
  </script>
  @if(app('request')->has('comment'))
    <script>
      $(document).on('load', function() {
        var elmnt = document.getElementById("comment");
        elmnt.scrollIntoView();
      });
    </script>
  @endif
  <script>
      function editComment(item)
      {
          event.preventDefault();
          let id = $(item).attr('data-id');
          let comment = $('#'+id);
          let editor = new Jodit('#comment'+id);
          editor.value = comment.html();
          $('#'+item.id).hide();
          let btn = $('<br><input type="button" value="Edit" class="btn btn-dark" id="editBtn'+id+'" onclick="javascript:submitEdit('+id+',($(\'#comment'+id+'\').html()))"/>');
          $('#delete'+id).hide().after(btn);
      };
      function submitEdit(id,val)
      {
          $.ajax({
              url: '{{url()}}/comment/'+id+'/edit',
              type: 'post',
              data: {
                  comment: val
              },
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              dataType: 'json',
              success: function (data) {
                  $('.jodit_container').remove();
                  $('#editBtn'+id).remove();
                  $('#edit'+id).show();
                  $('#delete'+id).show()
                  $('#comment'+id).show()
              }
          });
      }
      function validateComment()
      {
        let text = document.getElementById('comment').value;
        if(text != '')
          document.getElementById('commentForm').submit()
        else
          alert('Comment Should not be empty')
      }

  </script>
@endsection
