@extends('layouts.front.master') @section('title','Contact Us | www.cybertech.com')

@section('css')

@endsection

@section('content')

  <div class="container pb-4">
<h3 class="text-center">Contact Us</h3>
<span class="pageTitleUnderline mb-4"></span>

    <div class="row pt-3">

      <div class="col-md-6">
        <h5><i class="fa fa-envelope"></i> Message Us<small><small class="required-input">&nbsp;(*required)</small></small>
        </h5>

        <form action="contactus" method="post" id="contact-form">
          {{csrf_field()}}
          <div class="form-group">
            <label for="from-name">Name</label><span class="required-input">*</span>
            <div class="input-group">
              {{--  <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-user-o"></i></span>
              </div>  --}}
              <input class="form-control" type="text" name="name" required="" placeholder="Full Name" id="from-name">
            </div>
          </div>
          <div class="form-group">
            <label for="from-email">Email</label><span class="required-input">*</span>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-envelope-o"></i></span>
              </div>
              <input class="form-control" type="text" name="email" required="" placeholder="Email Address" id="from-email">
            </div>
          </div>

          <div class="form-group">
            <label for="from-subject">Subject</label><span class="required-input">*</span>
            <div class="input-group">
              
              <input class="form-control" type="text" name="subject" required="" placeholder="Subject" id="from-subject">
            </div>
          </div>



            <div class="form-group">
              <label for="from-comments">Comments</label>
              <textarea class="form-control" rows="5" name="comment" placeholder="Enter Comments" id="from-comments"></textarea>
            </div>
            
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block btn-sm btn-signin" >Submit
              </button>
            </div>

          </form>
        </div>


        <div class="col-md-4 offset-md-2">
          <h5><i class="fa fa-user"></i> Our Email </h5>
              <div><a href="mailto:info@ideaspies.com">info@ideaspies.com</a></div>
              <!-- <div><span>www.awebsite.com</span></div> -->
              <hr class="d-sm-none d-md-block d-lg-none">

        </div>
      </div>

    </div>

  @endsection

@section('js')
  <script>
    let key = "{{env('RE_CAP_SITE')}}";
    $(document).ready(function () {
      grecaptcha.ready(function() {
        grecaptcha.execute(key, {action: 'homepage'}).then(function(token) {
          $('#contact-form').prepend("<input type=\"hidden\" name=\"recaptcha_token\" value=" + token + ">");
        });
      });
    });

    // var opts = {
    //   lines: 9, // The number of lines to draw
    //   length: 31, // The length of each line
    //   width: 31, // The line thickness
    //   radius: 43, // The radius of the inner circle
    //   scale: 0.15, // Scales overall size of the spinner
    //   corners: 1, // Corner roundness (0..1)
    //   color: '#ffffff', // CSS color or array of colors
    //   fadeColor: 'transparent', // CSS color or array of colors
    //   speed: 1, // Rounds per second
    //   rotate: 0, // The rotation offset
    //   animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
    //   direction: 1, // 1: clockwise, -1: counterclockwise
    //   zIndex: 2e9, // The z-index (defaults to 2000000000)
    //   className: 'spinner', // The CSS class to assign to the spinner
    //   top: '50%', // Top position relative to parent
    //   left: '50%', // Left position relative to parent
    //   shadow: '0 0 1px transparent', // Box-shadow for the lines
    //   position: 'absolute' // Element positioning
    // };
    //
    // var spinner = new Spinner(opts);
    $('#contact-form').submit(function () {
      event.preventDefault();
      $('.btn-signin').attr('disabled',true);
      let datas = $(this).serializeArray();
      let data = {};
      let url = $(this).attr('action');
      datas.forEach(function (item,index)
      {
        data[item.name] = item.value;
      });
      // let target = document.getElementById('contact-div');
      // spinner.spin(target);
      $.post(url,data).done(function (response) {
        // spinner.stop();
        if(JSON.parse(response).success == true)
          alert('Contact form submitted Successfully')
        else
          alert('Form submit Failed. Please refresh the page and try again')
        window.location.reload();
      })
    });
  </script>
@endsection
