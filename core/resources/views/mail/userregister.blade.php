<p>Hello {{$first_name}}</p>

<p>Thank you for registering and welcome to IdeaSpies.</p>

<p>As well as searching, sharing and rating ideas already posted you can now add clever ideas you see, including the idea behind your own startup if you have one. You can also comment on ideas and receive IdeaSpies Weekly.</p>

<p>Idea Spies are people who are observant, They see and share clever ideas happening around them to help make the world a better place, We’re pleased that you are observant and have the same purpose in sharing innovation to inspire action.</p>

<p><a href="https://www.ideaspies.com/enterprise/" target="_blank">IdeaSpies Enterprise</a> is the private version of IdeaSpies if your organisation would like to encourage ideas from employees and/or clients.</p>

<p>These <a href="https://www.ideaspies.com/guidelines/" target="_blank">guidelines</a> may be helpful in posting ideas.</p>


<p>After you've posted an idea remember to rate it and share it to your social media. Add @IdeaSpies so we can re-share it.</p>

<p>If you have any suggestions for making IdeaSpies more useful please let me know.</p>
<p>
	<li style="margin-left:0px">Best wishes</li>
	<li style="margin-left:0px">Lynn</li>
	<li style="margin-left:0px">Chief Idea Spy</li>
	<li style="margin-left:0px">lynn.wood@IdeaSpies.com</li>
	<li style="margin-left:0px">+61 418966625</li>

</p>
