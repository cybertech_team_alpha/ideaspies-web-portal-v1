<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

	'facebook' => [
	    'client_id' => env("FACEBOOK_CLIENT_ID",'1049333088599443'),
	    'client_secret' => env("FACEBOOK_CLIENT_SECRET",'d84cdaf60b68a9745523a2c365d5910a'),
	    'redirect' => 'https://ideaspies.com/auth/facebook/callback',
	],
	'google' => [
	    'client_id' => env("GOOGLE_CLIENT_ID",'748373689826-sceut00hov6hsdl0gulok6uicnojsvgo.apps.googleusercontent.com'),
	    'client_secret' => env("GOOGLE_CLIENT_SECRET",'sTJ71q2mr8rf8b4eG0kWO4NU'),
	    'redirect' => 'https://ideaspies.com/auth/google/callback',
	],
    'twitter' => [
        'client_id' => env("TWITTER_CLIENT_ID",'c5czYP7y93Cm7vgyhlcNs3Bnq'),
        'client_secret' => env("TWITTER_CLIENT_SECRET",'yWpEbUm1eWVno4TLSOmfDO2VSBfUB9dpZRbhQUF0aG2YPJL5xY'),
        'redirect' => 'https://ideaspies.com/auth/twitter/callback',
    ],
    'linkedin' => [
        'client_id' => env("LINKEDIN_CLIENT_ID",'863790o4j0fqz0'),
        'client_secret' => env("LINKEDIN_CLIENT_SECRET",'es9uJwBs3nz2UDp0'),
        'redirect' => 'https://ideaspies.com/auth/linkedin/callback',
    ],
	'usps' => [
		'username' => "114SOFTA1630"
	]

];
