<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Packages\PostManage\Models\Post;

class subscribed_user extends Model
{
    protected $table = "subscribed_users";

    protected $fillable = ['id','user_id','email'];

}
