<?php

namespace App\Http\Controllers;

use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Permissions\Models\Permission;
use App\Models\Category;
use JonnyW\PhantomJs\Client;
use Sentinel;
use Mail;
use Redirect;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use PostManage\Models\PostNotificationView;
// use CategoryManage\Models\Category;

use \PostManage\Models\Post;
use \SettingManage\Models\Setting;
use OpenGraph;
use Thumbnail;

class PostsController extends Controller
{
    public function viewImage()
    {
        return view('front.posts.post-with-image');
    }

    public function viewVideo()
    {
        return view('front.posts.post-with-video');
    }

    public function viewUrl()
    {
        return view('front.posts.post-with-url');
    }

    public function add(Request $request)
    {


    //   $validator = Validator::make(Input::all(),  Post::Rules(), Post::$messages);
    //  if ($validator->fails())
    //  {
    //     return \Redirect::back()->withErrors($validator)->withInput();
    // }
    // else{
    	
      $this->validate($request, [
        'content' => 'required_if:type,image,video',
        'category_id' => 'required',
        'type' => 'required',
        'title' => 'required',
        'video' => 'required_if:type,video|mimes:mp4,mov,ogg,qt,webm',
        'image' => 'required_if:type,image|mimes:jpeg,bmp,png',
        'url' => 'required_if:type,url|url',
      ]);

    //   }
    //   $validator = Validator::make(Input::all(),  Post::Rules(), Post::$messages);
    //  if ($validator->fails())
    //  {
    //     return \Redirect::back()->withErrors($validator)->withInput();
    // }
    // else{
      //   $this->validate($request, [
      //   'content' => 'required_if:type,image,video',
      //   'category_id' => 'required',
      //   'type' => 'required',
      //   'title' => 'required',
      //   'video' => 'required_if:type,video|mimes:mp4,mov,ogg,qt,webm',
      //   'image' => 'required_if:type,image|mimes:jpeg,bmp,png',
      //   'url' => 'required_if:type,url|url', 
      // ]);

      $requestData = $request->all();
      if ($request->type === 'image') {
        $path = 'uploads/images';
        $fileName = $this->saveFile($request->image, $path);
        $fullPath = 'core/storage/'.$path.'/'.$fileName;

        $requestData['img_path'] = $fullPath;

      }

        if ($request->type === 'url')
        {
          $data = get_meta_tags($request->url);
          $url = file_get_contents($request->url);

          $url = $request->url;
          $og = OpenGraph::fetch($url);
          
          array_key_exists('image',$og) ? $data['og:image'] = $og['image'] : '';
          array_key_exists('description',$og) ? $data['og:description'] = $og['description'] : '';
          $path = 'uploads/images';          
          
          switch (true)
          {
            case (array_key_exists('twitter:image',$data)):
              $name = 'file-' . date('YmdHis') ;
              $fileName = pathinfo($data['twitter:image']);
              if(array_key_exists('extension',$fileName))
              {
                if(strpos($fileName['extension'],'?')!== false)
                {
                  $extension = substr($fileName['extension'],0,strpos($fileName['extension'],'?'));
                }
                else
                {
                  $extension = $fileName['extension'];
                }
              }
              else {
                $extension = 'jpg';
              }
              $fullPath = 'core/storage/'.$path.'/'.$name.'.'.$extension;
              file_put_contents($fullPath,file_get_contents($data['twitter:image']));
              break;
            case (array_key_exists('og:image',$data)):
              $name = 'file-' . date('YmdHis') ;
              $fileName = pathinfo($data['og:image']);
              if(array_key_exists('extension',$fileName))
              {
                if(strpos($fileName['extension'],'?')!== false)
                {
                  $extension = substr($fileName['extension'],0,strpos($fileName['extension'],'?'));
                }
                else
                {
                  $extension = $fileName['extension'];
                }
              }
              else {
                $extension = 'jpg';
              }
              $fullPath = 'core/storage/'.$path.'/'.$name.'.'.$extension;
              file_put_contents($fullPath,file_get_contents($data['og:image']));
              break;
            default :
              $fullPath = 'assets/front/images/no_image.png';
              break;
          }
          $requestData['img_path'] = $fullPath;

          switch(true)
          {
            case (array_key_exists('description',$data)):
              $requestData['content'] = $data['description'];
              break;
            case (array_key_exists('twitter:description',$data)):
              $requestData['content'] = $data['twitter:description'];
              break;
            case (array_key_exists('og:description',$data)):
              $requestData['content'] = $data['og:description'];
              break;
            default: 
              break; 
          }
        }


      if ($request->type === 'video') {
        $path = 'uploads/videos';
        $fileName = $this->saveFile($request->video, $path);
        $fullPath = 'core/storage/'.$path.'/'.$fileName;
        // if fileName create thumbnail
          if($fileName){
              $destinationPath = storage_path('uploads/images');
              $imagePage = 'uploads/images';
              $thumbnail_image  = 'thumbnail-' . date('YmdHis') . '.jpg';
              $time_to_image    =  5;//floor(($duration)/2);
              $thumbnail_status = Thumbnail::getThumbnail($fullPath,$destinationPath,$thumbnail_image,$time_to_image);
              if($thumbnail_status)
              {
                  $thumbnailFullPath = 'core/storage/'.$imagePage.'/'.$thumbnail_image;
                  $requestData['img_path'] = $thumbnailFullPath;
              }
          }
        $requestData['video_path'] = $fullPath;
      }

      $requestData['added_by_type'] = 'user';
      $requestData['added_by_id'] = Sentinel::getUser()->id;

      $post = Post::create($requestData);

      if (!empty($request->tags) && $request->tags !== null) {
        $post->tag($request->tags);
      }

      $user = Sentinel::getUser();
      $posting = $post->toArray();
    //  dd($posting);
      $emails = ['$user->email','Setting::find(1)->admin_email'];
      Mail::send('mail.create-post', $posting, function ($message) use ($user) {
  //      $message->to(Setting::find(1)->admin_email);
          $message->to($user->email);
          $message->subject('New Idea');
      });

      return redirect('/')->with([
        'success' => true,
        'success.message' => 'Post Created successfully!',
        'success.title' => 'Well Done!'
      ]);

  }


    public function saveFile($file, $path)
    {
      $extn = $file->getClientOriginalExtension();
      $destinationPath = storage_path($path);
      $fileName = 'file-' . date('YmdHis') . '.' . $extn;
      $file->move($destinationPath, $fileName);
      return $fileName;
    }

    public function preview(Request $request){
      $data = get_meta_tags($request->url);
      $sites_html = file_get_contents($request->url);
      
      $url = $request->url;
      $og = OpenGraph::fetch($url);
      
      array_key_exists('image',$og) ? $data['og:image'] = $og['image'] : '';
      array_key_exists('description',$og) ? $data['og:description'] = $og['description'] : '';
      $path = 'uploads/images';           
          
      switch (true)
      {
        case (array_key_exists('twitter:image',$data)):
          $result['image'] = $data['twitter:image'];
          break;
        case (array_key_exists('og:image',$data)):
          $result['image'] = $data['og:image'];
          break;
        default :
          $fullPath = 'assets/front/images/no_image.png';
          break;
      }

      switch(true)
      {
        case (array_key_exists('description',$data)):
          $result['description'] = $data['description'];
          break;
        case (array_key_exists('twitter:description',$data)):
          $result['description'] = $data['twitter:description'];
          break;
        case (array_key_exists('og:description',$data)):
          $result['description'] = $data['og:description'];
          break;
        default:
          break; 
      }
          return $result;
    }

    public function deletePost($id)
    {
      $post = Post::find($id);
      if(Sentinel::check() && $post->added_by_id == Sentinel::check()->id)
      {
        $post->delete();
        // return redirect()->back();
        return response(['ok' => true]);
      }
      else
        // return redirect()->back();
        return response(['fail' => true]);
    }


    public function tagToArray($str)
  {
    preg_match_all('/#([^\s]+)/', $str, $matches);
    return $matches[1];
  }

  public function seen()
  {
    $user = Sentinel::getUser();

    $unseenPost = Post::unseenPosts();

    $seen = [];

    foreach ($unseenPost as $key => $value) {
      $seen[] = ['post_id' => $value->id, 'user_id' => $user->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
    }

    PostNotificationView::insert($seen);
  }

  public function notification()
  {
    $posts = Post::with('addedUser')->orderBy('id', 'desc')->paginate(10);

    return view('PostManage::notification', compact('posts'));
  }

  public function ideaOfTheWeek(Request $request)
  {
      $post = Post::find($request->id);
      $status = 0;

      if ($post->idea_of_the_week == 0) {
      //  dump('aaa');
        Post::where('id', '<>', $request->id)->update(['idea_of_the_week' =>0]);
        $status = 1;
      }

      $post->update(['idea_of_the_week' => $status]);
      return response('success',200);
  }

  public function markRedirect($id)
  {

      $post = Post::find($id);

      if ($post->id) {
        $user = Sentinel::getUser();
        $seen =['post_id' => $id, 'user_id' => $user->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];;
        PostNotificationView::insert($seen);
        return redirect('post/'.$id);
      }

      return back();
  }

  public function jsonList(Request $request)
  {
    if ($request->ajax()) {
      $user = Sentinel::getUser();

      $data = new Post();
      $data = $data->get();

      if(!$user->hasAccess(['admin'])){
        $data = Post::where('added_by_id', $user->id)->get();
      }

      $jsonList = array();
      $i = 1;
      foreach ($data as $key => $post) {

        $dd = array();
        array_push($dd, $post->id);
        array_push($dd, sprintf($post->title));
        array_push($dd, sprintf($post->category->name));
        array_push($dd, sprintf($post->visit_count));

        $view = '<a href="'.url('post/'.$post->id).'" class="disabled" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>';
        $postOfTheWeek = "";

        $permissions = Permission::whereIn('name', ['post.edit', 'admin', 'user'])->where('status', '=', 1)->lists('name');
        if (Sentinel::hasAnyAccess($permissions)) {
          $edit = '<a href="#" class="blue" onclick="window.location.href=\'' . url('admin/post/edit/' . $post->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a>';
        } else {
          $edit = '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>';
        }

        $permissions = Permission::whereIn('name', ['post.delete', 'admin', 'user'])->where('status', '=', 1)->lists('name');
        if (Sentinel::hasAnyAccess($permissions)) {
          $delete = '<a href="#" class="product-delete" onclick="confirmAction('. $post->id .')" data-id="' . $post->id . '" data-toggle="tooltip" data-placement="top" title="Delete Status"><i class="fa fa-trash-o"></i></a>';
          
        } else {
          $delete = '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>';
        }
        
        $permissions = Permission::whereIn('name', ['admin'])->where('status', '=', 1)->lists('name');

        if( Sentinel::hasAnyAccess($permissions)) {
          $checked = $post->idea_of_the_week ? 'checked' : '';
          $postOfTheWeek = '<input type="checkbox" onclick="markIdeaOfTheWeek('.$post->id.')"  title="make this as the idea if the week" class="mark" '.$checked.' data-toggle="toggle">';
        }

        array_push($dd, $postOfTheWeek. $edit . "&nbsp;&nbsp; ". $view );
        array_push($jsonList, $dd);
        $i++;
      }
      return response()->json(array('data' => $jsonList));
    } else {
      return response()->json(array('data' => []));
    }
  }
}
