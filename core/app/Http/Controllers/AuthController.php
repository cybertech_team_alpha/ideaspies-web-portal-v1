<?php namespace App\Http\Controllers;

use App\SubscribedUser;
use Input;
use Sentinel;
use Session;
use Socialite;
use DB;
use Validator;
use Illuminate\Http\Request;
use Mail;
use UserManage\Models\User;
use UserRoles\Models\UserRole;
use App\Models\Category;
class AuthController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Welcome Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('auth');
	}


	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function loginView() {
		try {
			if (!Sentinel::check()) {
				return view('layouts.back.login');
			} else {
				$redirect = Session::get('loginRedirect', '');
				Session::forget('loginRedirect');
				return redirect($redirect);
			}
		} catch (\Exception $e) {
			return view('layouts.back.login')->withErrors(['login' => $e->getMessage()]);
		}
	}

	public function registerLoginView()
	{
		if (!Sentinel::check()) {
			return view('front.login')->with('data');
		}

		$redirect = Session::get('loginRedirect', '');
		Session::forget('loginRedirect');
		return redirect($redirect);
	}

	/**
	 * Online user Registration data to database
	 *
	 * @return Redirect to online user add
	 */
	public function userRegister(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email|unique:users,email',
			'name' => 'required|max:255',
			'password' => 'required|confirmed|min:6'
		]);

		if (!empty($request->upload))
        {
            $path = 'uploads/images';
            $fileName = $this->saveFile($request->upload, $path);
            $fullPath = 'core/storage/'.$path.'/'.$fileName;

            $user = Sentinel::registerAndActivate([
                'email' => $request->email,
                'username' => $request->email,
                'password' => $request->password,
                'first_name' => $request->name,
                'img_path' => $fullPath
            ]);
        }
        else
        {
            $user = Sentinel::registerAndActivate([
                'email' => $request->email,
                'username' => $request->email,
                'password' => $request->password,
                'first_name' => $request->name,
            ]);
        }

        if($request->subscription_status == 'on')
        {
            $user->subscription_status = 1;
            $user->save();
            SubscribedUser::create(['user_id'=> $user->id,'email'=>$user->email]);
        }

        $user->makeRoot();

		$role = Sentinel::findRoleById(1);
		$role->users()->attach($user);

		User::rebuild();

		Sentinel::login($user);

					//  die("sssssaaaa");
		$user = User::where('email', Input::get('email'))->first()->toArray();
					// die($user);
	//	dd($user);
		Mail::send('mail.userregister', $user, function($message) use ($user)
			         {
			             $message->from('info@ideaspies.com', "Ideaspies.com");
			             $message->subject("New user regitered");
			             $message->to($user['email']);
			         });

		Mail::send('mail.adminregister', $user, function($message) use ($user)
					 			   {
					 			       $message->from('info@ideaspies.com', "Ideaspies.com");
					 			       $message->subject("New user regitered");
					 			       $message->to('info@ideaspies.com');
					 			   });

					//return $user
		return redirect('/');
	}

	public function sendEmail($thisUser)
	{

        //die($thisUser->email);

		$token = $thisUser->varifyToken;
		$email = $thisUser->email;
		$url = route('user.varifiedEmail', ['email' => $email, 'token' => $token]);

		// Mail::send('emails.varifyEmail', ['url' => $url], function ($message) use ($thisUser) {
		// 	$message->to($thisUser->email, '')->subject('Email Confirmation for Sweet Delights Cakery Account');
		// });
//
	}

	public function varifiedEmail($email, $token)
	{

		$users = DB::table('users')->where(['email' => $email, 'varifyToken' => $token])->first();

		if ($users) {
			DB::table('users')->where(['email' => $email, 'varifyToken' => $token])->update(['regStatus' => '1', 'varifyToken' => null]);
			return redirect('user/logout');
		} else {
			return 'User Not Found';
		}
	}

	public function login(Request $request) {

		$username = $request->has('username') ? $request->username : $request->email;

		$credentials = array(
			'username' => $username,
			'password' => Input::get('password'),
		);

		if (Input::get('remember')) {
			$remember = true;
		} else {
			$remember = false;
		}


		try {

			$user = Sentinel::authenticate($credentials, $remember);

			if ($user) {

				// if ($user->active === 0) {

				// }


	           if($user->hasAnyAccess(['front'])){
					$redirect = Session::get('loginRedirect', 'admin');
				}else{
					$redirect = Session::get('loginRedirect', 'admin');
				}
				Session::forget('loginRedirect');

				return redirect($redirect);

			}

			if ($request->has('username')) {
				  $user_email = $request->username;
				  $user1=DB::table('users')->where('username',$user_email)->get();
				  if (count($user1) != 0) {
					  if ($user1[0]->status == 2) {
								  return redirect()->back()->with(['error' => 'Your account is deactivated by the admin']);
								  }
				  }
			    return redirect('user/login')->with(['error' => 'Invalid email or password. Please try again']);
			}

			return redirect($redirect);

		} catch (\Exception $e) {
			return $msg = $e->getMessage();
		}


	}


	public function login_front(Request $request) {

		$username = $request->has('username') ? $request->username : $request->email;

		$credentials = array(
			'username' => $username,
			'password' => Input::get('password'),
		);

		if (Input::get('remember')) {
			$remember = true;
		} else {
			$remember = false;
		}

        $redirect = Session::pull('loginRedirect', '/');

		try {

			$user = Sentinel::authenticate($credentials, $remember);

			if ($user) {
				return redirect($redirect);
			}

			if ($request->has('email')) {
				$user_email = $request->email;
				$user2=DB::table('users')->where('username',$user_email)->get();
				if (count($user2) != 0) {
				   if ($user2[0]->wp_id != 0 && $user2[0]->wp_status == 0) {
						 $email = \request('email');
						 Session::put('userId',$email);
				//	 dd($email);
					   return redirect()->back()->with('popup', 'open');
				   }
				}
				return redirect('login')->with(['error' => 'Invalid email or password. Please try again']);
			}

			return redirect($redirect);

		} catch (\Exception $e) {
			return $msg = $e->getMessage();
		}


	}
	/*
		*	@method logout()
		*	@description Logging out the logged in user
		*	@return URL redirection
	*/
	public function logout() {
		Sentinel::logout();
		return redirect()->to('/');

	}


     /**
     * Redirect the user to the FACEBOOK authentication page.
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
 	/**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function redirectToTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    /**
     * Obtain the user information from FACEBOOK.
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();
//        dd($user);
        $user_details=$user->user;
        if(User::where('email',$user_details['email'])->exists()){
        	$fbloged_user=User::where('email',$user_details['email'])->first();
            if (empty($fbloged_user->fb_id))
            {
                $fbloged_user->fb_id = $user_details['id'];
                $fbloged_user->save();
            }
        	$user_login = Sentinel::findById($fbloged_user->id);
			Sentinel::login($user_login);
        	return redirect('/');
        }else{
        	try {
				$registed_user=DB::transaction(function ()  use ($user_details){

					$user = Sentinel::registerAndActivate([
						'email' =>$user_details['email'],
						'first_name' => $user_details['name'],
						'username' => $user_details['name'],
						'password' => $user_details['id'],
						'fb_id' => $user_details['id']
					]);

					if (!$user) {
						throw new TransactionException('', 100);
					}

					$user->makeRoot();

					$role = Sentinel::findRoleById(1);
					$role->users()->attach($user);

					User::rebuild();
					return $user;


				});
				Sentinel::login($registed_user);
        		return redirect('/');

			} catch (TransactionException $e) {
				if ($e->getCode() == 100) {
					Log::info("Could not register user");

					return redirect('user/register')->with(['error' => true,
						'error.message' => "Could not register user",
						'error.title' => 'Ops!']);
				}
			} catch (Exception $e) {

			}
        }






        // $user->token;
    }/**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleTwitterCallback()
    {
	     $user = Socialite::driver('twitter')->user();
	     $user_details=$user->user;

       if(User::where('email',$user_details['email'])->exists()){

	    	$fbloged_user=User::where('email',$user_details['email'])->first();
	    	$user_login = Sentinel::findById($fbloged_user->id);
	    	if (empty($fbloged_user->tw_id))
            {
                $fbloged_user->tw_id = $user_details['id'];
                $fbloged_user->save();
            }
			Sentinel::login($user_login);
	    	return redirect('/');
        }else{

        	try {
				$registed_user=DB::transaction(function ()  use ($user_details){

					$user = Sentinel::registerAndActivate([
						'email' =>$user_details['email'],
						'username' => $user_details['given_name'],
						'first_name' => $user_details['given_name'],
						'last_name' => $user_details['family_name'],
						'password' => $user_details['id'],
						'tw_id' => $user_details['id']
					]);

					if (!$user) {
						throw new TransactionException('', 100);
					}

					$user->makeRoot();

					$role = Sentinel::findRoleById(1);
					$role->users()->attach($user);

					User::rebuild();
					return $user;


				});
				Sentinel::login($registed_user);
        		return redirect('/');

			} catch (TransactionException $e) {
				if ($e->getCode() == 100) {
					Log::info("Could not register user");

					return redirect('user/register')->with(['error' => true,
						'error.message' => "Could not register user",
						'error.title' => 'Ops!']);
				}
			} catch (Exception $e) {

			}
        }

    }

    public function handleLinkedinCallback()
    {
        $user = Socialite::driver('linkedin')->user();
        $user_details=$user->user;
//	     dd($user);

        if(User::where('email',$user_details['emailAddress'])->exists()){

            $fbloged_user=User::where('email',$user_details['emailAddress'])->first();
            $user_login = Sentinel::findById($fbloged_user->id);
            if (empty($fbloged_user->in_id))
            {
                $fbloged_user->in_id = $user_details['id'];
                $fbloged_user->save();
            }
            Sentinel::login($user_login);
            return redirect('/');
        }else{

            try {
                $registed_user=DB::transaction(function ()  use ($user_details){

                    $user = Sentinel::registerAndActivate([
                        'email' =>$user_details['email'],
                        'username' => $user_details['firstName'],
                        'first_name' => $user_details['firstName'],
                        'last_name' => $user_details['lastName'],
                        'password' => $user_details['id'],
                        'in_id' => $user_details['id']
                    ]);

                    if (!$user) {
                        throw new TransactionException('', 100);
                    }

                    $user->makeRoot();

                    $role = Sentinel::findRoleById(1);
                    $role->users()->attach($user);

                    User::rebuild();
                    return $user;


                });
                Sentinel::login($registed_user);
                return redirect('/');

            } catch (TransactionException $e) {
                if ($e->getCode() == 100) {
                    Log::info("Could not register user");

                    return redirect('user/register')->with(['error' => true,
                        'error.message' => "Could not register user",
                        'error.title' => 'Ops!']);
                }
            } catch (Exception $e) {

            }
        }

    }

    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->user();
        $user_details=$user->user;
//	     dd($user);

        if(User::where('email',$user_details['email'])->exists()){

            $fbloged_user=User::where('email',$user_details['email'])->first();
            $user_login = Sentinel::findById($fbloged_user->id);
            if (empty($fbloged_user->g_id))
            {
                $fbloged_user->g_id = $user_details['id'];
                $fbloged_user->save();
            }
            Sentinel::login($user_login);
            return redirect('/');
        }else{

            try {
                $registed_user=DB::transaction(function ()  use ($user_details){

                    $user = Sentinel::registerAndActivate([
                        'email' =>$user_details['email'],
                        'username' => $user_details['given_name'],
                        'first_name' => $user_details['given_name'],
                        'last_name' => $user_details['family_name'],
                        'password' => $user_details['id'],
                        'g_id' => $user_details['id']
                    ]);

                    if (!$user) {
                        throw new TransactionException('', 100);
                    }

                    $user->makeRoot();

                    $role = Sentinel::findRoleById(1);
                    $role->users()->attach($user);

                    User::rebuild();
                    return $user;


                });
                Sentinel::login($registed_user);
                return redirect('/');

            } catch (TransactionException $e) {
                if ($e->getCode() == 100) {
                    Log::info("Could not register user");

                    return redirect('user/register')->with(['error' => true,
                        'error.message' => "Could not register user",
                        'error.title' => 'Ops!']);
                }
            } catch (Exception $e) {

            }
        }

    }

    public function saveFile($file, $path)
    {
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $fileName = 'file-' . date('YmdHis') . '.' . $extn;
        $file->move($destinationPath, $fileName);
        return $fileName;
    }

		public function wp_reset_password(Request $request)
		{
			$email = $request->session()->get('userId');
			$user1=DB::table('users')->where('email',$email)->first();
			$id = $user1->id;
			$user = User::find($id);
// 		dd($user);
//		$password = $request->password;
//		dd($password);
      $request->session()->forget('userId');
      $user->password = bcrypt($request->password);
			$user->wp_status = 1 ;
			$user->update();

			return back()->with('Successmsg', 'Password reset successfully!');;
		}
}
