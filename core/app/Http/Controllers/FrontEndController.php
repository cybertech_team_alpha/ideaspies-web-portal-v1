<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use UserManage\Models\User;
use Validator;
use Mail;
use  Sentinel;
use Session;
use DB;

class FrontEndController extends Controller
{
    public function about()
    {
        return view('front.about-us');
    }

    public function privacy()
    {
        return view('front.privacy');
    }

    public function terms()
    {
        return view('front.terms');
    }

    public function enterprise()
    {
        return view('front.enterprise');
    }

    public function tips()
    {
        return view('front.top-tips');
    }

    public function guidelines()
    {
        return view('front.guidelines');
    }

    public function login()
    {
        return view('front.login');
    }

    public function contact()
    {
        return view('front.contact-us');
    }

    public function register()
    {
        return view('front.register');
    }

    public function forget()
    {
        return view('front.reset-password');
    }

    public function forgetSubmit(Request $request)
    {
          $validator = Validator::make($request->all(), [
              'email' => 'required|exists:users,email',
          ]);

          if ($validator->fails()) {
              return redirect()
                  ->back()
                  ->withErrors($validator)
                  ->withInput();
          }
            $user_email = $request->email;
            $user=DB::table('users')->where('email',$user_email)->get();
      //    dd($user);
            if ($user[0]->status == 2) {
              $msg = 'Your account is deactivated by admin.';
              return redirect()->back()->with('message',$msg);
            }
          else
          {
              $user = User::where('email',$request->email)->first();
              $reminder = Reminder::create($user);
              $url = url('processReset?token='.base64_encode($reminder->code).'&user='.$user->id);
              $mailData = ['user'=> $user->first_name, 'url'=>$url];
              $msg = 'Check your email to reset your password.';
              Mail::send('mail.reset', $mailData, function ($message) use ($user) {
                  $message->to($user->email);
                  $message->subject('Password Reset - Ideaspies');
              });
              return redirect()->back()->with('message',$msg);
          }
    }

    public function reset()
    {
        $token = base64_decode(\request('token'));
        $id = \request('user');

        Session::put('token',$token);
        Session::put('userId',$id);

        return view('front.reset-verify');
    }

    public function processReset(Request $request,$user)
    {
        $id = $request->token;
        $password = $request->password;
        $user = Sentinel::findById($user);
        $data = ['password' => str_random(16), 'user' => $user->first_name];
        $request->session()->forget('token');
        $request->session()->forget('userId');
        if ($reminder = Reminder::complete($user, $id, $password))
        {
            Mail::send('mail.reset-successful', $data, function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('Password Reset Success - Ideaspies');
            });

            Reminder::removeExpired();
            Sentinel::login($user);
            return redirect('/');
        }
        else
        {
            return redirect()->back()
                ->with('message','Password Reset Failed');
        }

    }
}
