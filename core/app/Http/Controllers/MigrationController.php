<?php

namespace App\Http\Controllers;
ini_set('max_execution_time', 0);

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use UserManage\Models\User;
use Sentinel;
use PostManage\Models\Post;
use PostManage\Models\PostRate;
use App\Comment;

class MigrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        // return 'k';
       $users = DB::connection('mysql2')->select("SELECT wp_users.ID AS wp_id,wp_users.user_email AS email,wp_users.display_name AS display_name,wp_wysija_user.firstname AS first_name,wp_wysija_user.lastname AS last_nmae from wp_users INNER JOIN wp_wysija_user ON wp_users.ID=wp_wysija_user.wpuser_id");

       foreach ($users as $key => $value) {
        $name='';
        if ($value->first_name !='') {
           $name=$value->first_name;
        }else{
           $name=$value->display_name;
        }
         $user = Sentinel::registerAndActivate([                             
                'email' => $value->email,
                'wp_id' => $value->wp_id,
                'username' => $value->email,
                'password' => '123123',
                'first_name' => $name,    
                'supervisor_id'=>1,
                'active'=>1
       
            ]);
            // $user->makeRoot();
            $role = Sentinel::findRoleById(1);
            $role->users()->attach($user);  
       }


    }
     public function post_old()
    {
        $category_array=array(
            '36'=>1,
            '37'=>2,
            '15'=>3,
            '19'=>4,
            '33'=>5,
            '29'=>6,
            '35'=>7,
            '24'=>8,
            '38'=>9,
            '20'=>10,
            '4'=>11,
        );
        // return 'k';
             $mainposts = DB::connection('mysql2')->select("SELECT * 
FROM   wp_posts 
       INNER JOIN wp_term_relationships 
               ON wp_posts.id = wp_term_relationships.object_id 
       INNER JOIN wp_term_taxonomy 
               ON wp_term_relationships.term_taxonomy_id = 
                  wp_term_taxonomy.term_taxonomy_id 
       INNER JOIN wp_terms 
               ON wp_term_taxonomy.term_id = wp_terms.term_id 
WHERE  (wp_posts.post_type = 'post' 
       OR wp_posts.post_type = 'page') 
       AND wp_posts.post_parent = 0 
       AND wp_terms.term_id IN ( 36, 37, 15, 19, 
                                 33, 29, 35, 24, 
                                 38, 20, 4 ) ");
        foreach ($mainposts as $key => $mainpost) {
             $attachment = DB::connection('mysql2')->select("SELECT * FROM wp_posts where wp_posts.post_type = 'attachment' AND wp_posts.post_parent=".$mainpost->ID);
            if ( $attachment) {
                    $finel_attachment=$attachment[0];
                    $path='';         
                    $type='';
                    $substr="https://www.ideaspies.com/wp-content/uploads/";
                    // echo $finel_attachment->guid;
                    
                    if (strpos($finel_attachment->guid, $substr) !== false) {
                        $finel_attachment->guid=str_replace($substr,'',$finel_attachment->guid);
                        $explode=explode('/', $finel_attachment->guid);
                        $folderpath=$explode[0].'/'.$explode[1];
                        $path='https://www.ideaspies.com/wp-content/uploads/'.$folderpath.'/'.$explode[2];
                    }else{
                         $explode=explode('-', $finel_attachment->post_date);
                          $folderpath=$explode[0].'/'.$explode[1];
                         
                          if ( $finel_attachment->post_mime_type=='image/png') {
                              $path='https://www.ideaspies.com/wp-content/uploads/'.$folderpath.'/'.$finel_attachment->post_title.'.png';
                          }else if ($finel_attachment->post_mime_type=='image/jpeg') {
                             $path='https://www.ideaspies.com/wp-content/uploads/'.$folderpath.'/'.$finel_attachment->post_title.'.jpeg';
                          }elseif ($finel_attachment->post_mime_type=='image/gif') {
                             $path='https://www.ideaspies.com/wp-content/uploads/'.$folderpath.'/'.$finel_attachment->post_title.'.gif';
                          }elseif ($finel_attachment->post_mime_type=='jpg') {
                             $path='https://www.ideaspies.com/wp-content/uploads/'.$folderpath.'/'.$finel_attachment->post_title.'.jpg';
                          }
                          
                    }
                    $user=User::where('wp_id', $mainpost->post_author)->first();
                    $post=null;
                    if ( $finel_attachment->post_mime_type=='image/png' |$finel_attachment->post_mime_type=='image/jpeg'|$finel_attachment->post_mime_type=='image/gif'|$finel_attachment->post_mime_type=='jpg') {
                         $post=Post::create([
                            'wp_id'=>$mainpost->ID,
                            'type'=>'image',
                            'content'=>$mainpost->post_content,
                            'img_path'=>$path,
                            'video_path'=>'',
                            'category_id'=>$category_array[$mainpost->term_id],
                            'added_by_id'=> $user->id,
                            'added_by_type'=>'user',
                            'title'=>$mainpost->post_title
                      ]);
                    }else{
                         $post=Post::create([
                            'wp_id'=>$mainpost->ID,
                            'type'=>'video',
                            'content'=>$mainpost->post_content,
                            'img_path'=>'',
                            'video_path'=>$path,
                            'category_id'=>$category_array[$mainpost->term_id],
                            'added_by_id'=> $user->id,
                            'added_by_type'=>'user',
                            'title'=>$mainpost->post_title
                      ]);
                    }
                    $rating = DB::connection('mysql2')->select("SELECT * FROM wp_ratings where wp_ratings.rating_postid=".$mainpost->ID);
                    foreach ($rating as $key => $rate) {
                       $user=User::where('wp_id', $rate->rating_userid)->first();
                       $user_id=0;
                       if ($user) {
                          $user_id=$user->id;
                       }
                       PostRate::create([
                        'user_id'=>$user_id,
                        'value'=>$rate->rating_rating,
                        'post_id'=>$post->id,
                        'wp_id'=>$rate->rating_id
                       ]);
                    }
                    $comments = DB::connection('mysql2')->select("SELECT * FROM wp_comments where wp_comments.comment_post_ID=".$mainpost->ID);
                    foreach ($comments as $key => $comment) {

                        Comment::create([
                          'comment'=>$comment->comment_content,
                          'user_id'=>$user_id,
                          'post_id'=>$comment->comment_post_ID,
                          'wp_id'=>$comment->comment_ID,
                          'status'=>$comment->comment_approved
                        ]);
                    }
            }
            
        
           

              // $user=User::where('wp_id', $mainpost->post_author)->first();
              // Post::create([
              //   'wp_id'=>$mainpost->ID,
              //   'type'=>'',
              //   'content'=>$mainpost->post_content,
              //   'img_path'=>$image_path,
              //   'video_path'=>$video_path,
              //   'category_id'=>$category_array[$mainpost->term_id],
              //   'added_by_id'=> $user->id,
              //   'added_by_type'=>'user',
              //   'title'=>$mainpost->post_title,
              // ]);
            
          //return $attachment = DB::connection('mysql2')->select("SELECT * FROM wp_posts where wp_posts.post_type = 'attachment' AND wp_posts.post_parent=".$mainpost->ID);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */






    public function post()
    {
        $category_array=array(
            '36'=>1,
            '37'=>2,
            '15'=>3,
            '19'=>4,
            '33'=>5,
            '29'=>6,
            '35'=>7,
            '24'=>8,
            '38'=>9,
            '20'=>10,
            '4'=>11,
        );
        $mainposts = DB::connection('mysql2')->select("SELECT * FROM wp_posts INNER JOIN wp_term_relationships ON wp_posts.id = wp_term_relationships.object_id INNER JOIN wp_term_taxonomy ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id INNER JOIN wp_terms ON wp_term_taxonomy.term_id = wp_terms.term_id WHERE (wp_posts.post_type = 'post' OR wp_posts.post_type = 'page') AND wp_posts.post_parent = 0 AND wp_terms.term_id IN ( 36, 37, 15, 19, 33, 29, 35, 24, 38, 20, 4 ) and wp_posts.ID > 13538 ORDER BY wp_posts.ID ASC");
        foreach ($mainposts as $key => $mainpost) {
          $user_check=User::where('wp_id', $mainpost->post_author)->first();
          if ($user_check) {
             $path='';
          $type='';
             $meta = DB::connection('mysql2')->select("SELECT * FROM wp_postmeta WHERE post_id =". $mainpost->ID ." and meta_key = '_thumbnail_id'");
            if ( $meta) {

               $attachment = DB::connection('mysql2')->select("SELECT * FROM wp_postmeta WHERE post_id =".$meta[0]->meta_value." AND meta_key='_wp_attached_file'" );
                $path='core/storage/uploads/images/'.$attachment[0]->meta_value;

            }
            $user_id=17;
            if ($mainpost->post_author!=0) {
               $user=User::where('wp_id', $mainpost->post_author)->first();
               $user_id=$user->id;
            }
            
             
              // urlpreviewbox url= "http://www.spinali-design.com/blogs/journal/launch-of-the-first-pair-of-vibrating-connected-jeans-1"
             
              $post=null;
              if( strpos($mainpost->post_content, 'urlpreviewbox') !== false){
                  $url_preview=  str_replace("urlpreviewbox url= ","",$mainpost->post_content);
                  $url = substr($url_preview, 1, -1);
                  $post=Post::create([
                      'wp_id'=>$mainpost->ID,
                      'type'=>'url',
                      'content'=>'',
                      'img_path'=>'',
                      'video_path'=>'',
                      'url'=>$url,
                      'category_id'=>$category_array[$mainpost->term_id],
                      'added_by_id'=> $user_id,
                      'added_by_type'=>'user',
                      'title'=>$mainpost->post_title,
                      'created_at'=>$mainpost->post_modified
                ]);
              }else{
              
                $post=Post::create([
                      'wp_id'=>$mainpost->ID,
                      'type'=>'image',
                      'content'=>$mainpost->post_content,
                      'img_path'=>$path,
                      'video_path'=>'',
                      'url'=>'',
                      'category_id'=>$category_array[$mainpost->term_id],
                      'added_by_id'=> $user_id,
                      'added_by_type'=>'user',
                      'title'=>$mainpost->post_title,
                      'created_at'=>$mainpost->post_modified
                ]);
              }

               $rating = DB::connection('mysql2')->select("SELECT * FROM wp_ratings where wp_ratings.rating_postid=".$mainpost->ID);
                    foreach ($rating as $key => $rate) {
                       $user=User::where('wp_id', $rate->rating_userid)->first();
                       $user_id=17;
                       if ($user) {
                          $user_id=$user->id;
                       }
                       PostRate::create([
                        'user_id'=>$user_id,
                        'value'=>$rate->rating_rating,
                        'post_id'=>$post->id,
                        'wp_id'=>$rate->rating_id
                       ]);
                    }
                    $comments = DB::connection('mysql2')->select("SELECT * FROM wp_comments where wp_comments.comment_post_ID=".$mainpost->ID);
                    foreach ($comments as $key => $comment) {

                        Comment::create([
                          'comment'=>$comment->comment_content,
                          'user_id'=>$user_id,
                          'post_id'=>$comment->comment_post_ID,
                          'wp_id'=>$comment->comment_ID,
                          'status'=>$comment->comment_approved,
                          'created_at'=>$comment->comment_date
                        ]);
                    }
          }
         
           

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */






    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
