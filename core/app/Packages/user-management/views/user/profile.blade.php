@extends('layouts.back.master') @section('current_title','Profile')
@section('css')
  <style media="screen">
  /* image uploader styles */
  .file-upload {
    position: relative;
    display: inline-block;
    width: 100%;
  }

  .file-upload__label {
    display: block;
    padding: 7px 15px;
    color: #888 !important;
    background: #fff;
    border-radius: 4px;
    transition: background .3s;
    border: 1px solid #ced4da;
    width: 100%;
  }
  .file-upload__label:hover {
    cursor: pointer;
    background: #ccc;
  }

  .file-upload__input {
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    font-size: 1;
    width: 0;
    height: 100%;
    opacity: 0;
  }
  /* image uploader styles end */
  .preview{
    max-height: 150px;
    margin-bottom: 15px;
    border-radius:
  }

      #upload-image{
          height: auto;
          width: auto;
          top: 0;
          bottom: 0;
          left: 0;
          right: 0;
          max-width: 150px;
          max-height: 150px;
          display: block;
          margin: auto  ;
      }
      .btn1{
        padding-left: 20px;
      }
      .btn2{
        padding: 0px;
      }
  </style>
@endsection
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin')}}">Dashboard</a></li>

        <li class="active">
            <span>Profile</span>
        </li>
    </ol>
</div>

@section('content')
  <div class="panel panel-default">
    <div class="panel-body">
      <ul class="nav nav-tabs">
  <li class="{{ !session()->has('tab') ? 'active' : '' }}"><a data-toggle="tab" href="#profile">Profile Details</a></li>
  @if (!Sentinel::getUser()->hasAccess(['admin']))
    <li><a data-toggle="tab" href="#menu2">Delete Account</a></li>
  @endif
</ul>

<div class="tab-content">
  <div id="profile" class="tab-pane fade  in {{ !session()->has('tab') ? 'active' : '' }}">
    {!! Form::open(['method' => 'POST', 'route' => 'user.admin.profile', 'class' => 'form-horizontal','files'=>'true']) !!}
    <br/>
        <div class="col-md-12">
          <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
              {!! Form::label('first_name', ' IDEA SPY NAME', ['class' => 'col-md-2 control-label']) !!}
              <div class="col-md-10">
                  {!! Form::text('first_name', $user->first_name, ['class' => 'form-control']) !!}
                  {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
              </div>
          </div>

          <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
              {!! Form::label('email', 'EMAIL', ['class' => 'col-md-2 control-label']) !!}
              <div class="col-md-10">
                  {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
                  {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
              </div>
          </div>
          <div class="form-group">
            <div class="{{ $errors->has('Profile_Picture') ? 'has-error' : ''}}">
              {!! Form::label('upload-image', 'PROFILE PICTURE', ['class' => 'col-md-2 control-label']) !!}
              <div class="col-md-10">
                  <div id="preview" class="preview float-left text-left"></div>
                  {!! Form::file('Profile_Picture',['class' => 'form-control','id'=>"upload"]) !!}
                  <small>Max 500KB</small>
                  {!! $errors->first('Profile_Picture', '<p class="help-block">:message</p>') !!}
              </div>
            </div>
          </div>
          <div class="form-group">
              {!! Form::label('upload-image', 'CURRENT PROFILE PICTURE', ['class' => 'col-md-2 control-label']) !!}
              <div class="col-md-10">
                <div class="row">
                  @if ($user->img_path != null)
                  <img src="{{asset($user->img_path)}}" class="img-circle m-b profile float-left" alt="logo" style="margin-left:10px;">
                  @else
                  <img src="{{asset('assets/front/images/avatar.png')}}" class="img-circle m-b profile" alt="logo" style="margin-left:10px;">
                  <small>No profile picture found</small>
                  @endif
                </div>
              </div>
          </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
              <div class="row">
                <div class="col-sm-2 btn1">
                  {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                  {!! Form::close() !!}
                </div>
                <div class="col-sm-2 btn2">
                  @if ($user->img_path != null)
                  {!! Form::open(['method' => 'POST', 'route' => 'user.admin.profile.delete', 'class' => 'form-horizontal']) !!}
                  {!! Form::submit('Delete profile picture', ['class' => 'btn btn-danger']) !!}
                  {!! Form::close() !!}
                  @else
                  @endif
                </div>
              </div>
            </div>
        </div>
    {!! Form::open(['method' => 'POST', 'route' => 'user.admin.password', 'class' => 'form-horizontal']) !!}
    <br/>
        <div class="col-md-12">
          <div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
              {!! Form::label('old_password', 'OLD PASSWORD*', ['class' => 'col-md-2 control-label']) !!}
              <div class="col-md-10">
                  {!! Form::password('old_password', ['class' => 'form-control']) !!}
                  {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
              </div>
          </div>
          <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
              {!! Form::label('password', 'NEW PASSWORD*', ['class' => 'col-md-2 control-label']) !!}
              <div class="col-md-10">
                  {!! Form::password('password', ['class' => 'form-control']) !!}
                  {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
              </div>
          </div>
          <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
              {!! Form::label('password_confirmation', 'RE-TYPE PASSWORD*', ['class' => 'col-md-2 control-label']) !!}
              <div class="col-md-10">
                  {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                  {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
              </div>
          </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-8">
                {!! Form::submit('Done', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
    {!! Form::close() !!}
  </div>
  <div id="menu2" class="tab-pane fabe">
      <div class="row" style="margin-top: 2em;">
        <div class="col-md-6 col-md-offset-3" >


                <p style="font-size:1.5rem">
                    <input type="checkbox" name="agree" required id=""> I am sure please delete my account</p>

                <a href="#" class="red user-delete" data-id="{{$user->id}}" data-toggle="tooltip" data-placement="top" title="Delete User"><i class="fa fa-trash-o"></i></a>


        </div>
      </div>
  </div>

</div>



    </div>
  </div>
@stop

@section('js')
  <script>
      var _URL = window.URL || window.webkitURL;

      $("#upload").click(function() {
        if ($('#upload-image').length)
          $('#upload-image').remove()
      });

      $("#upload").change(function () {
          var old = $("#upload").clone();
          var file;
          var image;
          if ((file = old[0].files[0])) {
              image = new Image();
              image.onload = function () {
                  $("#preview").append(this);
                  $(this).attr("id", "upload-image");
              }
              image.src = _URL.createObjectURL(file);
          }
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function(){
        $('.user-delete').click(function(e){
              e.preventDefault();
              id = $(this).data('id');
              confirmAlert(id);

        });
      });

      function confirmAction(id){
        $.ajax({
          method: "POST",
          url: '{{url('user/delete')}}',
          data:{ 'id' : id  },
          dataType: 'json',
          success : function(response) {
          swal(response.msg);
      //  location.reload();
          }
      });
    }
  </script>
  @endsection
