@extends('layouts.back.master')
@section('current_title','Edit Post')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/styles/jquery.tag-editor.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
@stop
@section('current_path')

<style type="text/css">
  h2.swal2-title {
    display: block!important;
  }
</style>
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/post/list')}}">Post Management</a></li>

        <li class="active">
            <span>Edit Post</span>
        </li>
    </ol>
</div>


@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                {!! Form::model($post, [ 'method' => 'PUT','onsubmit'=>'urlSubmit(this,event)','url' => ['/admin/post/edit', $post->id], 'class' => 'form-horizontal', 'files' => 'true','id'=>'form' ]) !!}

                    <div class="form-group">
                        <div class="{{ $errors->has('type') ? 'has-error' : ''}}">
                            {!! Form::label('type', 'Type *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::select('type',  ['content only' => 'content only','image' => 'image','video' => 'video','url' => 'url'], null, ['class' => 'form-control', 'required' => 'required', 'id' => 'postType']) !!}
                                {!! $errors->first('type', '<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group post-types" data-type="url" {{ $post->type === 'url' ? '' : 'style=display:none' }}>
                        <div class="{{ $errors->has('url') ? 'has-error' : ''}}">
                            {!! Form::label('url', 'URL *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::url('url',null, ['class' => 'form-control col-sm-10']) !!}
                                {!! $errors->first('url','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group post-types" data-type="video" {{ $post->type === 'video' ? '' : 'style=display:none' }}>
                        <div class="{{ $errors->has('video') ? 'has-error' : ''}}">
                            {!! Form::label('video', 'New Video *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::file('video', ['class' => 'form-control col-sm-10','id'=>'timer']) !!}
                                <small>Max one Mins & Max 30Mb file. </small>
                                <p id="timing" class="alert alert-danger" style="display:none">Please upload video less than One Mins length & Less than 30Mb file.</p>
                                {!! $errors->first('video','<p class="help is-danger"></p>') !!}
                                <div class="col-md-6"  style="margin-top:10px;">
                                    @if ($post->type === 'video')
                                        <video controls src="{{ asset($post->video_path) }}" style="width:500px;height:300px;"></video>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group post-types" data-type="image" {{ $post->type === 'image' ? '' : 'style=display:none' }}>
                        <div class="{{ $errors->has('image') ? 'has-error' : ''}}">
                            {!! Form::label('image', 'New Image *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::file('image', ['class' => 'form-control col-sm-10','id'=>'file']) !!}
                                <small>Max 5000KB</small>
                                <small> <p id="img_det" class="alert alert-danger" style="display:none"></p> </small>
                                {!! $errors->first('image','<p class="help is-danger">:message</p>') !!}

                                <div class="col-md-6">
                                    @if ($post->type === 'image')
                                        <img src="{{ asset($post->img_path) }}"  class="img-responsive" alt="">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="{{ $errors->has('title') ? 'has-error' : ''}}">
                            {!! Form::label('title', 'Titile *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('title',null, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!}
                                <small>Max 100 Characters</small>
                                {!! $errors->first('title','
                                <p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="{{ $errors->has('category_id') ? 'has-error' : ''}}">
                            {!! Form::label('category_id', 'Category *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required' => 'required']) !!} {!! $errors->first('category_id','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="description">
                    {{-- <div class="form-group"> --}}
                        <div class="{{ $errors->has('content') ? 'has-error' : ''}}">
                            {!! Form::label('content', 'Description *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::textarea('content',null, ['class' => 'form-control col-sm-10']) !!}
                                {{-- {!! Form::textarea('content',null, ['class' => 'form-control col-sm-10', 'required' => 'required']) !!} --}}
                                {!! $errors->first('content','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>



                {!! Form::close() !!}

                    <span class="form-horizontal">



                    <div class="form-group">
                        <div class="{{ $errors->has('tags') ? 'has-error' : ''}}">
                            {!! Form::label('tags', 'Tags *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('tags',null, ['class' => 'form-control col-sm-10', 'placeholder' => '#hash #tags', 'required' => 'required', 'data-role'=>"tagsinput"]) !!}
                                {!! $errors->first('tags','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                            <button class="btn btn-primary" id="submitBtn">Done</button>
                        </div>
                    </div>

                    </span>
            </div>
        </div>
    </div>


@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/back/scripts/jquery.caret.min.js')}}"></script>
    <script src="{{asset('assets/back/scripts/jquery.tag-editor.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script type="text/javascript">
      function urlSubmit(url,e)
      {
        console.log(e);
        e.preventDefault();
        if(e.target[3].value == '')
          e.target.submit();
        else
        {
          $.ajax({
            url : "{{url('previewUrl')}}",
            cache : false,
            data : {url : e.target[3].value},
            success : function(response)
            {
              let description = response.description;
              let image = response.image;
              Swal.fire({
              title: e.target[6].value,
              width:600,
              padding:'3em',
              text: description,
              imageUrl: image,
              // imageWidth: 400,
              // imageHeight: 200,
              animation: false,
              confirmButtonText:  'Confirm',
              cancelButtonText:  'Cancel',
              showCancelButton: true,
              showConfirmButton: true
            })
            .then((result) => {
              if (result.value) {
                e.target.submit();
              }
              else if(result.dismiss)
              $('#submitBtn').removeAttr('disabled');
            })
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
              alert(errorThrown+'. URL not supported.');
              $('#submitBtn').removeAttr('disabled');
            }

          });
        }
      };
    </script>
    <script type="text/javascript">

    $(document).ready(function () {

        console.log();

        showSelected($('#postType').val())



       $('#postType').on('change', event => {
           const selectedValue = event.target.value


           showSelected(selectedValue)

       })



       $("#category_id").select2()

       $('#tags').tagEditor({ initialTags: {!! json_encode($tags) !!} })

              $('#submitBtn').click(e => {

           let tags = $('#tags').tagEditor('getTags')[0].tags

           console.log(tags);


           tags.forEach((element,index) => {
               console.log('aa');

               $('form').append('<input name="tags['+index+']" hidden value="'+element+'">');
           });

           $('form').submit()
       })
})

function showSelected(selectedValue) {
    const type = $('.post-types')

    type.each(el => {

        let element = $(type[el])

        console.log(element.data('type'));
//    console.log();


        if (element.data('type') == selectedValue) {
            element.show()
        }else{
            element.hide()
        }


    })
}
    </script>
    @if ($user = Sentinel::getUser())
    {
        @if ($user->inRole('administrator'))
        {
          <script type="text/javascript">
          $('textarea').each(function () {
              var editor = new Jodit(this, {
                  "autofocus": false,
                  "language": "en",
                  "enter": "P",
                  "buttons": ",,,,,,,,,ul,ol,|,font,fontsize,|,image,video,table,link,|,align,undo,redo,\n,",
                  "limitWords": true | 100,
              });
              editor.events.on('afterInsertImage', function (image) {
                  image.style = "max-height:150px;";
              })
          });
          </script>
        }
        @endif
    }
    @endif

    <script type="text/javascript">
    var vid = document.createElement('video');
          document.querySelector('#timer').addEventListener('change', function() {
          // create url to use as the src of the video
          var fileURL = URL.createObjectURL(this.files[0]);
          vid.src = fileURL;
          // wait for duration to change from NaN to the actual duration
          vid.ondurationchange = function() {
            //alert(this.duration);
            if (this.duration>60) {
              $("#submitBtn").hide();
              $("#timing").show();
            } else {
              $("#submitBtn").show();
              $("#timing").hide();
            }
          };
          });
    </script>
    <script type="text/javascript">
    window.onload = function() {
            event.preventDefault();
            var opt = $("#postType").val()
       //     alert(opt)
             if(opt == 'url'){
                 $('#description').hide();
             }else{
                 $('#description').show();
             }
           };
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
       $("#postType").on('click',function(event){
          event.preventDefault();
          var opt = $("#postType").val()
     //     alert(opt)
           if(opt == 'url'){
               $('#description').hide();
           }else{
               $('#description').show();
           }
       });
   });
    </script>

    <script type="text/javascript">
     $(document).ready(function(){
        $("#submitBtn").on('click',function(){
                $("#submitBtn").hide();
                $(document).ready(function() {
                $.blockUI({
                    message: '<h1><img src={{asset('assets/back/images/loading-bars.svg')}} /> Please wait!</h1>',
                });
            });
            });
        });
     </script>

     <script type="text/javascript">

     var _URL = window.URL || window.webkitURL;

     $("#file").change(function(e) {
     var file, img;


 if ((file = this.files[0])) {
     img = new Image();
     img.onload = function() {
     $("#img_det").show();
     document.getElementById("img_det").innerHTML = "uploaded image Width: " +this.width + "px" + " uploaded image Height: " +this.height + "px";
     if (this.height<175 || this.width<225) {
       swal("Error!", "The minimum height should be 175px and minimum width should be 225px", "error");
       $('#submitBtn').prop('disabled',true)
     }
     else {
       $('#submitBtn').prop('disabled',false)
     }
     };
     img.onerror = function() {
         alert( "not a valid file: " + file.type);
     };
     img.src = _URL.createObjectURL(file);


 }

 });
     </script>
@stop
