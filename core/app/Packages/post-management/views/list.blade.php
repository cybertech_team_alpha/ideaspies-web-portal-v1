@extends('layouts.back.master')
@section('current_title','All Post')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }
    input.mark {
        margin-right: 10px;
    }
</style>

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 30px;
  height: 17px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 13px;
  width: 13px;
  left: 3px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(11px);
  -ms-transform: translateX(11px);
  transform: translateX(11px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/post/list')}}">Post Management</a></li>

        <li class="active">
            <span>Post List</span>
        </li>
    </ol>
</div>

@stop
@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('admin/post/add')}}';">
    <p class="plus">+</p>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="17%">#</th>
                            <th class="text-center" style="font-weight:normal;">Title</th>
                            <th class="text-center" style="font-weight:normal;">Category</th>
                            <th class="text-center" width="13%" style="font-weight:normal;">Visit Count</th>
                            <th class="text-center" width="13%" style="font-weight:normal;">Activate/Deactivate</th>
                            <th class="text-center" width="12%" style="font-weight:normal;">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">

    var table;
	$(document).ready(function(){


        table=$('#example1').dataTable( {
            "ajax": '{{url('admin/post/json/list')}}',
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
      //      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            "lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
            buttons: [
                {extend: 'copy',className: 'btn-sm'},
                {extend: 'csv',title: 'Post List', className: 'btn-sm'},
                {extend: 'pdf', title: 'Post List', className: 'btn-sm'},
                {extend: 'print',className: 'btn-sm'}
            ],
             "autoWidth": false,
        });

        table.on( 'draw.dt', function () {
            $('.Post-delete').click(function(e){
                  e.preventDefault();
                  id = $(this).data('id');
                  confirmAlert(id);
            });
            $(".check-active-deactivate").change(function(e) {
              e.preventDefault();
              var id = $(this).val();
              var check = 0;
                if (this.checked) {
                  check =1;
                }else {
                  check =0;
                }

                // alert(id);

                $.ajax({
                  type:'post',
                  url: '{{url('admin/post/active-deactivate')}}',
                  data: 'id=' + id+'&check='+check,
                  dataType:'JSON',
                  success:function(data){
                    toastr.success(data.msg, data.title);
                    // location.reload();
                    console.log(res);
                    }
                });
            });
        });
	});

  function confirmAction(id){
    $.ajax({
      method: "POST",
      url: '{{url('admin/post/delete')}}',
      data:{ 'id' : id  }
    })
      .done(function( msg ) {
        table.fnReloadAjax();
      });

  }

  function markIdeaOfTheWeek(id) {


    $.ajax({
      method: "POST",
      url: "{{url('admin/post/idea-of-the-week')}}",
      data:{ 'id' : id  }
    })
    .done(function( msg ) {
        toastr["success"]('Status updated successfully','Idea of the Week')
        table.fnReloadAjax();
    });
  }

</script>

@stop
