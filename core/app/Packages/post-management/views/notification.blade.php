@extends('layouts.back.master')
@section('current_title','All Notifications')
@section('css')

<style type="text/css">
    .notify-row{
        margin: 2em;
        /* margin-bottom: 2em; */
        /* margin-top: 2em; */
    }
    .notify-row div {

        padding: 10px;
    }
    .notify-row div:hover {
        background-color: #dddddd;
    }

    .seen{
        background-color: ;
    }

    .unseen {
        background-color: #f3f2f2;
    }
</style>



@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/post/list')}}">Post Management</a></li>

        <li class="active">
            <span>Notifications</span>
        </li>
    </ol>
</div>


@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <button class="btn btn-primary btn-sm pull-right" id="markAll" style="margin-bottom: 1em">
                    Mark all as read
                </button>
            </div>
            <div class="panel-body">

                <div class="container">
                    @foreach ($posts as $item)
                        <a href="{{ url('admin/post/mark-and-redirect/'.$item->id) }}">
                            <div class="row notify-row {{ $item->isUnseen() ? 'seen' : 'unseen' }}">
                                <div class="col-md-12">
                                  <div class="row">
                                    <div class="col-md-8">
                                        idea <b>{{ $item->title }}</b> posted by <b>{{ $item->addedUser->name }}</b>
                                        <br><small>{{ $item->created_at }}</small>
                                    </div>
                                    @if ($item->isUnseen())
                                    @else
                                    <div class="col-md-4">
                                       <span class="badge badge-success"><i class="fa fa-bell"></i> New</span>
                                    </div>
                                    @endif
                                  </div>

                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        {!! $posts->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop
@section('js')
<script>

    $('#markAll').click(function () {

        $.ajax({
            method: "POST",
            url: '{{url('admin/post/seen')}}',
        })
        .done(function( msg ) {
            $('.post-count').html('0')
            location.reload();
        });

     })
</script>

@stop
