@extends('layouts.back.master')
@section('current_title','New Post')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/styles/jquery.tag-editor.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

<style>
.ui-sortable{
   border-color: #e4e5e7 !important;
}
iframe{
  border:1px solid #acdae8;;
}

  h2.swal2-title {
    display: block!important;
  }
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/post/list')}}">Post Management</a></li>

        <li class="active">
            <span>New Post</span>
        </li>
    </ol>
</div>

@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <form method="POST" class="form-horizontal" id="form" enctype="multipart/form-data" onsubmit="urlSubmit(this,event)">
                    {!!Form::token()!!}

                    <div class="form-group">
                        <div class="{{ $errors->has('type') ? 'has-error' : ''}}">
                            {!! Form::label('type', 'Type *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::select('type', ['image' => 'image','video' => 'video','url' => 'url'], null, ['class' => 'form-control','required' => 'required', 'id' => 'postType','placeholder'=>'Please select a type from dropdown']) !!}
                                {!! $errors->first('type', '<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group post-types" data-type="url" style="display:none" id="ur">
                        <div class="{{ $errors->has('url') ? 'has-error' : ''}}">
                            {!! Form::label('url', 'URL *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::url('url',null, ['class' => 'form-control col-sm-10']) !!} {!! $errors->first('url','
                                <p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group post-types" data-type="video" style="display:none" id="vd">
                        <div class="{{ $errors->has('video') ? 'has-error' : ''}}">
                            {!! Form::label('video', 'Video *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::file('video', ['class' => 'form-control col-sm-10','id'=>'timer']) !!}
                                <small>Min 5 seconds & 1Mb file<br/>Max 1 Minute & 30Mb file</small>
                                <p id="timing" class="alert alert-danger" style="display:none">Please upload video less than one Mins length & 30Mb file</p>
                                {!! $errors->first('video','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>



                    <div class="form-group post-types" data-type="image" style="display:none" id="im">
                        <div class="{{ $errors->has('image') ? 'has-error' : ''}}">
                            {!! Form::label('image', 'Image *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::file('image', ['class' => 'form-control col-sm-10','id'=>'file']) !!}
                                <small>Max 5MB</small>
                                <small> <p id="img_det" class="alert alert-danger" style="display:none"></p> </small>
                                {!! $errors->first('image','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="{{ $errors->has('title') ? 'has-error' : ''}}">
                            {!! Form::label('title', 'Title *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('title',null, ['class' => 'form-control col-sm-10', 'required' => 'required','id'=>'title'])
                                !!}
                                <small>Max 100 Characters</small>
                                {!! $errors->first('title','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="{{ $errors->has('category_id') ? 'has-error' : ''}}">
                            {!! Form::label('category_id', 'Category *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required' => 'required','placeholder' => 'Please select an option']) !!}
                                {!! $errors->first('category_id', '<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="description">
                        {{-- <div class="form-group"> --}}
                        <div class="{{ $errors->has('content') ? 'has-error' : ''}}">
                            {!! Form::label('content', 'Description *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::textarea('content',null, ['class' => 'max','id'=>'editor']) !!}
                                {!! $errors->first('content','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>

                    </form>


                    <span class="form-horizontal">



                    <div class="form-group">
                        <div class="{{ $errors->has('tags') ? 'has-error' : ''}}">
                            {!! Form::label('tags', 'Tags *', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('tags',null, ['class' => 'form-control col-sm-10', 'placeholder' => '#hash #tags', 'required' => 'required', 'data-role'=>"tagsinput"]) !!}
                                {!! $errors->first('tags','<p class="help is-danger">:message</p>') !!}
                            </div>
                        </div>
                    </div>





                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                            <button class="btn btn-primary" id="submitBtn">Done</button>
                        </div>
                    </div>

                    </span>


            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="container-fluid" style="padding:10px;">
              <div class="form-group">
                  <div class="text-left">
                      Description
                  </div>
                  <div class="">
                      <textarea id="description1" class="form-control" readonly></textarea>
                  </div>
              </div>
              <iframe
                  id="url1"
                  width="100%"
                  height="350px"
                  frameborder="0"
                  allow="autoplay; encrypted-media"
                  allowfullscreen>
              </iframe>
              <div class="col-sm-12" style="padding:0px">
                      <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
      </div>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/back/scripts/jquery.caret.min.js')}}"></script>
    <script src="{{asset('assets/back/scripts/jquery.tag-editor.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script type="text/javascript">
    function urlSubmit(url,e)
      {
        console.log(e);
        e.preventDefault();
        if(e.target[2].value == '')
          e.target.submit();
        else
        {
          $.ajax({
            url : "{{url('previewUrl')}}",
            cache : false,
            data : {url : e.target[2].value},
            success : function(response)
            {
              let description = response.description;
              let image = response.image;
              Swal.fire({
              title: e.target[5].value,
              width:600,
              padding:'3em',
              text: description,
              imageUrl: image,
              // imageWidth: 400,
              // imageHeight: 200,
              animation: false,
              confirmButtonText:  'Confirm',
              cancelButtonText:  'Cancel',
              showCancelButton: true,
              showConfirmButton: true
            })
            .then((result) => {
              if (result.value) {
                e.target.submit();
              }
              else if(result.dismiss)
              $('#submitBtn').removeAttr('disabled');
            })
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
              alert(errorThrown+'. URL not supported.');
              $('#submitBtn').removeAttr('disabled');
            }

          });
        }
      };
    </script>
    <script type="text/javascript">
       const type = $('.post-types')

       $('#postType').on('change', event => {
           const selectedValue = event.target.value

           showSelected(selectedValue)

       })


       $("#category_id").select2()

       $('#tags').tagEditor()


       $('#submitBtn').click(e => {

           let tags = $('#tags').tagEditor('getTags')[0].tags

           console.log(tags);


           tags.forEach((element,index) => {
               console.log('aa');

               $('#form').append('<input name="tags['+index+']" hidden value="'+element+'">');
           });

           $('form').submit()
       })

function showSelected(selectedValue) {
    const type = $('.post-types')

    type.each(el => {

        let element = $(type[el])

        console.log(element.data('type'));
//    console.log();


        if (element.data('type') == selectedValue) {
            element.show()
        }else{
            element.hide()
        }


    })
}

$(document).ready(function(){
     $("#preview").on('click', function(event) {
       event.preventDefault();
       var getDescription = $("#content").val();
       $("#description1").html(getDescription);
       var adr=$("#url").val();
       $("#url1").attr('src', adr);
   });
 });

 $(document).ready(function(){
    $("#postType").on('click',function(event){
       event.preventDefault();
       var opt = $("#postType").val()
  //     alert(opt)
        if(opt == 'url'){
            $("#preview").show();
            $('#description').hide();
        }else{
            $("#preview").hide();
            $('#description').show();
        }
    });
});

$(window).on('load',function(){
var opt = $("#postType").val()
if(opt == 'url'){
    $("#preview").show();
    $("#ur").show();
}else{
    $("#preview").hide();
    $("#ur").hide();
}
if(opt == 'image'){
    $("#im").show();
}else{
    $("#im").hide();
}
if(opt == 'video'){
    $("#vd").show();
}else{
    $("#vd").hide();
}
});

    </script>
    @if ($user = Sentinel::getUser())
    {
        @if ($user->inRole('administrator'))
        {
          <script type="text/javascript">
          $('textarea').each(function () {
              var editor = new Jodit(this, {
                  "autofocus": false,
                  "language": "en",
                  "enter": "P",
                  "buttons": ",,,,,,,,,ul,ol,|,font,fontsize,|,image,video,table,link,|,align,undo,redo,\n,",
                  "limitWords": true | 100,
		           //   "limitChars": true | 40,
              });
              editor.events.on('afterInsertImage', function (image) {
                  image.style = "max-height:150px;";
              })
          });
          </script>
        }
        @endif
    }
    @endif
    <script type="text/javascript">
    var vid = document.createElement('video');
          document.querySelector('#timer').addEventListener('change', function() {
          // create url to use as the src of the video
          var fileURL = URL.createObjectURL(this.files[0]);
          vid.src = fileURL;
          // wait for duration to change from NaN to the actual duration
          vid.ondurationchange = function() {
            //alert(this.duration);
            if (this.duration>60) {
              $("#submitBtn").hide();
              $("#timing").show();
            } else {
              $("#submitBtn").show();
              $("#timing").hide();
            }
          };
          });
    </script>
s
   <script type="text/javascript">
    $(document).ready(function(){
       $("#submitBtn").on('click',function(){
               $("#submitBtn").hide();
               $(document).ready(function() {
               $.blockUI({
                   message: '<h1><img src={{asset('assets/back/images/loading-bars.svg')}} /> Please wait!</h1>',
               });
           });

           });
       });
    </script>
    <script type="text/javascript">

    var _URL = window.URL || window.webkitURL;

    $("#file").change(function(e) {
    var file, img;


if ((file = this.files[0])) {
    img = new Image();
    img.onload = function() {
    $("#img_det").show();
    document.getElementById("img_det").innerHTML = "uploaded image Width " +this.width + "px" + " and Height " +this.height + "px";
    if (this.height<175 || this.width<225) {
      swal("Error!", "The minimum height should be 175px and minimum width should be 225px", "error");
      $('#submitBtn').prop('disabled',true);
    }
    else {
      $('#submitBtn').prop('disabled',false)
    }
    };
    img.onerror = function() {
        alert( "not a valid file: " + file.type);
    };
    img.src = _URL.createObjectURL(file);


}

});
    </script>
@stop
