<?php

namespace PostManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use PostManage\Models\Post;
use CategoryManage\Models\Category;
use PostManage\Models\PostNotificationView;
use Carbon\Carbon;
use SettingManage\Models\Setting;
use Mail;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use Thumbnail;

class PostController extends Controller {


    /**
     * Show the Branch add screen to the user.
     *
     * @return Response
     */
    public function addView()
    {

        $categories = Category::lists('name','id')->toArray();

        // dd($categories);
        return view('PostManage::add', compact('categories'));
    }

    /**
     * Add new Branch data to database
     *
     * @return Redirect to Brach add
     */
    public function add(Request $request)
    {
        ini_set('memory_limit','64M');
        $validator = Validator::make(Input::all(),  Post::Rules(), Post::$messages);
        if ($validator->fails())
        {
            //    dd($validator);
            return \Redirect::back()->withErrors($validator)->withInput(Input::except('tags'));
        }
        else{
            $requestData = $request->all();
//    dd($requestData);
            if ($request->type === 'image') {
                $path = 'uploads/images';
                $fileName = $this->saveFile($request->image, $path);
                $fullPath = 'core/storage/'.$path.'/'.$fileName;
                $requestData['img_path'] = $fullPath;

            }

            if ($request->type === 'video') {
                $path = 'uploads/videos';
                $fileName = $this->saveFile($request->video, $path);
                $fullPath = 'core/storage/'.$path.'/'.$fileName;

                // if fileName create thumbnail
                if($fileName){
                    $destinationPath = storage_path('uploads/images');
                    $imagePage = 'uploads/images';
                    $thumbnail_image  = 'thumbnail-' . date('YmdHis') . '.jpg';
                    $time_to_image    =  5;//floor(($duration)/2);
                    $thumbnail_status = Thumbnail::getThumbnail($fullPath,$destinationPath,$thumbnail_image,$time_to_image);
                    if($thumbnail_status)
                    {
                        $thumbnailFullPath = 'core/storage/'.$imagePage.'/'.$thumbnail_image;
                        $requestData['img_path'] = $thumbnailFullPath;
                    }
                }

                $requestData['video_path'] = $fullPath;
            }
            if ($request->type === 'url')
            {
                $data = get_meta_tags($request->url);
                $url = file_get_contents($request->url);

                $html = new \DOMDocument();
                @$html->loadHTML($url);
                foreach($html->getElementsByTagName('meta') as $meta) {
                    if($meta->getAttribute('property')=='og:image'){
                        $data['og:image'] = $meta->getAttribute('content');
                    }
                    if($meta->getAttribute('property')=='og:description'){
                        $data['og:description'] = $meta->getAttribute('content');
                    }
                }
                $path = 'uploads/images';

                switch (true)
                {
                    case (array_key_exists('twitter:image',$data)):
                        $name = 'file-' . date('YmdHis') ;
                        $fileName = pathinfo($data['twitter:image']);
                        if(strpos($fileName['extension'],'?')!== false)
                        {
                            $extension = substr($fileName['extension'],0,strpos($fileName['extension'],'?'));
                        }
                        else
                        {
                            $extension = $fileName['extension'];
                        }
                        $fullPath = 'core/storage/'.$path.'/'.$name.'.'.$extension;
                        file_put_contents($fullPath,file_get_contents($data['twitter:image']));
                        break;
                    case (array_key_exists('og:image',$data)):
                        $name = 'file-' . date('YmdHis') ;
                        $fileName = pathinfo($data['og:image']);
                        if(strpos($fileName['extension'],'?')!== false)
                        {
                            $extension = substr($fileName['extension'],0,strpos($fileName['extension'],'?'));
                        }
                        else
                        {
                            $extension = $fileName['extension'];
                        }
                        $fullPath = 'core/storage/'.$path.'/'.$name.'.'.$extension;
                        file_put_contents($fullPath,file_get_contents($data['og:image']));
                        break;
                    default :
                        $fullPath = 'assets/front/images/no_image.png';
                        break;
                }
                $requestData['img_path'] = $fullPath;

                switch(true)
                {
                    case (array_key_exists('description',$data)):
                        $requestData['content'] = $data['description'];
                        break;
                    case (array_key_exists('twitter:description',$data)):
                        $requestData['content'] = $data['twitter:description'];
                        break;
                    case (array_key_exists('og:description',$data)):
                        $requestData['content'] = $data['og:description'];
                        break;
                    default:
                        break;
                }
            }
            $requestData['added_by_type'] = 'admin';
            $requestData['added_by_id'] = Sentinel::getUser()->id;

            $post = Post::create($requestData);

            if (!empty($request->tags) && $request->tags !== null) {
                $post->tag($request->tags);
            }
            $user = Sentinel::getUser();
            $posting = $post->toArray();
            //  dd($posting);
            $emails = ['$user->email','Setting::find(1)->admin_email'];
            Mail::send('mail.create-post', $posting, function ($message) use ($user) {
//      $message->to(Setting::find(1)->admin_email);
                $message->to($user->email);
                $message->subject('New Idea');
            });

            return redirect('admin/post/index')->with([
                'success' => true,
                'success.message' => 'Post Created successfully!',
                'success.title' => 'Well Done!'
            ]);
        }
    }

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView()
    {
        return view('PostManage::list');
    }

    public function index()
    {
        return view('PostManage::index');
    }

    public function test()
    {
        $user = Sentinel::getUser();
        $posts = Post::where('added_by_id',$user->id)->get();
        return view('PostManage::test',compact('posts'));
    }

    public function allPosts(Request $request)
    {
        $user = Sentinel::getUser();
        $columns = array(
            0 =>'id',
            1 =>'title',
            2=> 'category',
            3=> 'visit_count',
            4=> 'actdeact',
            5=> 'edit',
            6=> 'delete',
            7=> 'view',
            8=> 'idea',
        );

        $totalData = Post::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            if(!$user->hasAccess(['admin'])){
                $posts = Post::offset($start)
                    ->where('added_by_id',$user->id)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else{
                $posts = Post::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
        }
        else {
            $search = $request->input('search.value');

            $posts =  Post::where('id','LIKE',"%{$search}%")
                ->orWhere('title', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Post::where('id','LIKE',"%{$search}%")
                ->orWhere('title', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                //            $show =  route('posts.show',$post->id);
                //            $edit =  route('posts.edit',$post->id);
                $permissions = Permission::whereIn('name', ['post.active-deactivate','admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $actdeact = '<label class="switch"> <input type="checkbox" value="'.$post->id.'" class="check-active-deactivate" '.(($post->status ==1) ? "checked" : "").'> <span class="slider round "></span></label>';

                } else {
                    $actdeact = '<span class="text-danger">Access denied</span>';
                }

                $permissions = Permission::whereIn('name', ['post.edit', 'admin', 'user'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $edit = '<a href="#" class="blue" onclick="window.location.href=\'' . url('admin/post/edit/' . $post->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a>';
                } else {
                    $edit = '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>';
                }
                $permissions = Permission::whereIn('name', ['post.delete', 'admin', 'user'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $delete = '<a href="#" class="Post-delete" data-id="' . $post->id . '" data-toggle="tooltip" data-placement="top" title="Delete Status"><i class="fa fa-trash-o"></i></a>';

                } else {
                    $delete = '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>';
                }
                $view = '<a href="'.url('post/'.$post->id).'" class="disabled" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>';

                $permissions = Permission::whereIn('name', ['admin','user'])->where('status', '=', 1)->lists('name');

                if( Sentinel::hasAnyAccess($permissions)) {
                    $checked = $post->idea_of_the_week ? 'checked' : '';
                    $postOfTheWeek = '<input type="checkbox" onclick="markIdeaOfTheWeek('.$post->id.')"  title="make this as the idea if the week" class="mark" '.$checked.' data-toggle="toggle">';
                }
                $nestedData['id'] = $post->id;
                $nestedData['title'] = $post->title;
                //      $nestedData['content'] = substr(strip_tags($post->content),0,50)."...";
                $nestedData['category'] = $post->category->name;
                $nestedData['visit_count'] = $post->visit_count;
                $nestedData['actdeact'] = $actdeact;
                $nestedData['edit'] = $edit;
                $nestedData['delete'] = $delete;
                $nestedData['view'] = $view;
                $nestedData['idea'] = $postOfTheWeek;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $user = Sentinel::getUser();

            $data = new Post();
            $data = $data->get();

            if(!$user->hasAccess(['admin'])){
                $data = Post::where('added_by_id', $user->id)->get();
            }
            else {
                $data = Post::all()->get();
            }
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $post) {

                $dd = array();
                array_push($dd, $post->id);
                array_push($dd, sprintf($post->title));
                array_push($dd, sprintf($post->category->name));
                array_push($dd, sprintf($post->visit_count));
                array_push($dd, '<label class="switch"> <input type="checkbox" value="'.$post->id.'" class="check-active-deactivate" '.(($post->status ==1) ? "checked" : "").'> <span class="slider round "></span></label>');
                // array_push($dd, sprintf($post->category->name ?? ''));

                $view = '<a href="'.url('post/'.$post->id).'" class="disabled" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>';

                $permissions = Permission::whereIn('name', ['post.active-deactivate','admin','user'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $actdeact = '<label class="switch"> <input type="checkbox" value="'.$post->id.'" class="check-active-deactivate" '.(($post->status ==1) ? "checked" : "").'> <span class="slider round "></span></label>';

                } else {
                    $actdeact = '<span class="text-danger">Access denied</span>';
                }

                $permissions = Permission::whereIn('name', ['post.edit', 'admin', 'user'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $edit = '<a href="#" class="blue" onclick="window.location.href=\'' . url('admin/post/edit/' . $post->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-pencil"></i></a>';
                } else {
                    $edit = '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>';
                }
                $permissions = Permission::whereIn('name', ['post.delete', 'admin', 'user'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $delete = '<a href="#" class="Post-delete" data-id="' . $post->id . '" data-toggle="tooltip" data-placement="top" title="Delete Status"><i class="fa fa-trash-o"></i></a>';

                } else {
                    $delete = '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>';
                }
                $view = '<a href="'.url('post/'.$post->id).'" class="disabled" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>';

                $permissions = Permission::whereIn('name', ['admin','user'])->where('status', '=', 1)->lists('name');

                if( Sentinel::hasAnyAccess($permissions)) {
                    $checked = $post->idea_of_the_week ? 'checked' : '';
                    $postOfTheWeek = '<input type="checkbox" onclick="markIdeaOfTheWeek('.$post->id.')"  title="make this as the idea if the week" class="mark" '.$checked.' data-toggle="toggle">';
                }

                array_push($dd, $postOfTheWeek. $edit . "&nbsp;&nbsp; ". $view ."&nbsp;&nbsp; " . $delete);
                array_push($jsonList, $dd);
                $i++;
            }
            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request)
    {
        {
            if($request->ajax()){
                $id = $request->input('id');

                $post = Post::find($id);
                if($post){
                    $post->status=4;
                    $post->deleted_at= Carbon::now();
                    $post->update();

                    return response()->json(['status' => 'success']);
                }else{
                    return response()->json(['status' => 'invalid_id']);
                }
            }else{
                return response()->json(['status' => 'not_ajax']);
            }
        }
    }
    /**
     * Show the devcie edit screen to the devcie.
     *
     * @return Response
     */
    public function editView($id)
    {
        $post = Post::findOrFail($id);
        $user = Sentinel::getUser();

        if (!$user->hasAccess(['admin']) && $user->id !== $post->added_by_id) {

            return redirect('admin/post/index');

        }

        $categories = Category::lists('name', 'id')->toArray();

        $tags = $post->tagNames();

        return view('PostManage::edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Add new device data to database
     *
     * @return Redirect to Branch add
     */
    public function edit(Request $request, $id)
    {

        $this->validate($request, [
            'content' => 'required',
            'category_id' => 'required|exists:categories,id',
            'type' => 'required',
            'video' => 'mimes:mp4,mov,ogg,qt,webm|max:26624',
            'image' => 'mimes:jpeg,bmp,png|max:5120',
            'url' => 'required_if:type,url|url',
            'tags' => 'array'
        ]);

        $post = Post::findOrFail($id);

        $user = Sentinel::getUser();


        if (!$user->hasAccess(['admin']) && $user->id !== $post->added_by_id) {

            return redirect('admin/post/index');

        }

        $requestData = $request->all();
//  dd($requestData);

        if ($request->type === 'image' && $request->hasFile('image')) {
            $path = 'uploads/images';
            $fileName = $this->saveFile($request->image, $path);
            $fullPath = 'core/storage/' . $path . '/' . $fileName;

            $requestData['img_path'] = $fullPath;

        }

        if ($request->type === 'video' && $request->hasFile('video')) {
            $path = 'uploads/videos';
            $fileName = $this->saveFile($request->video, $path);
            $fullPath = 'core/storage/' . $path . '/' . $fileName;
            // if fileName create thumbnail
            if($fileName){
                $destinationPath = storage_path('uploads/images');
                $imagePage = 'uploads/images';
                $thumbnail_image  = 'thumbnail-' . date('YmdHis') . '.jpg';
                $time_to_image    =  10;//floor(($duration)/2);
                $thumbnail_status = Thumbnail::getThumbnail($fullPath,$destinationPath,$thumbnail_image,$time_to_image);
                if($thumbnail_status)
                {
                    $thumbnailFullPath = 'core/storage/'.$imagePage.'/'.$thumbnail_image;
                    $requestData['img_path'] = $thumbnailFullPath;
                }
            }

            $requestData['video_path'] = $fullPath;
        }

        $requestData['added_by_type'] = 'admin';
        $requestData['added_by_id'] = Sentinel::getUser()->id;


        $post->update($requestData);
        $post->untag();

        if (!empty($request->tags) && $request->tags !== null) {
            $post->tag($request->tags);
        }

        return redirect('admin/post/edit/' . $id)->with([
            'success' => true,
            'success.message' => 'Post updated successfully!',
            'success.title' => 'Good Job!'
        ]);
    }


    public function saveFile($file, $path)
    {
        $extn = $file->getClientOriginalExtension();
        $destinationPath = storage_path($path);
        $fileName = 'file-' . date('YmdHis') . '.' . $extn;
        $file->move($destinationPath, $fileName);
        return $fileName;
    }

    public function tagToArray($str)
    {
        preg_match_all('/#([^\s]+)/', $str, $matches);
        return $matches[1];
    }

    public function seen()
    {
        $user = Sentinel::getUser();

        $unseenPost = Post::unseenPosts();

        $seen = [];

        foreach ($unseenPost as $key => $value) {
            $seen[] = ['post_id' => $value->id, 'user_id' => $user->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
        }

        PostNotificationView::insert($seen);
    }

    public function notification()
    {
        $posts = Post::with('addedUser')->orderBy('id', 'desc')->paginate(10);

        return view('PostManage::notification', compact('posts'));
    }

    public function ideaOfTheWeek(Request $request)
    {
        $post = Post::find($request->id);
        $status = 0;

        if ($post->idea_of_the_week == 0) {
            //  dump('aaa');
            Post::where('id', '<>', $request->id)->update(['idea_of_the_week' =>0]);
            $status = 1;
        }

        $post->update(['idea_of_the_week' => $status]);
        return response('success',200);
    }

    public function markRedirect($id)
    {

        $post = Post::find($id);

        if ($post->id) {
            $user = Sentinel::getUser();
            $seen =['post_id' => $id, 'user_id' => $user->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];;
            PostNotificationView::insert($seen);
            return redirect('post/'.$id);
        }

        return back();
    }

    public function postActivateDeactivate(Request $request)
    {
        $id = $request->id;
        $check = $request->check;
        $post = Post::find($id);
        // dd($product);
        if($post){
            if ($check == 1) {
                // activate
                $post->status = 1;
                $post->save();
                return response()->json([
                    'succes'=>true,
                    'msg'=>'Activated post '.$post->name,
                    'title' => 'Good Job!'
                ]);

            }else {
                $post->status = 2;
                $post->save();
                return response()->json([
                    'succes'=>true,
                    'msg'=>'Deactivated post '.$post->name,
                    'title' => 'Good Job!'
                ]);
            }

        }
    }
}
