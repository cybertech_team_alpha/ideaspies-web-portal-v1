<?php
namespace PostManage\Models;

use Illuminate\Database\Eloquent\Model;
use CategoryManage\Models\Category;
use UserManage\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sentinel;

class Post extends Model
{
    use SoftDeletes;
    use \Conner\Tagging\Taggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wp_id',
        'parent_id',
        'type', //'image','url','content'
        'content',
        'img_path',
        'video_path',
        'url',
        'category_id',
        'added_by_type', //'admin', 'user'
        'added_by_id',
        'title',
        'idea_of_the_week',
        'status',
    ];

     protected $dates = ['deleted_at'];

    public static function Rules(){
       $rules= array(
         'category_id'=>'required|exists:categories,id',
//       'content' => 'required',
         'type' => 'required',
         'title' => 'required|max:100',
         'video' => 'required_if:type,video|mimes:mp4,mov,ogg,qt,webm|max:30720‬',
         'image' => 'required_if:type,image|mimes:jpeg,bmp,png|max:5120',
         'url' => 'required_if:type,url|url',
         'tags' => 'array'
       );
      return $rules;
   }

   public static  $messages=array(
        'category_id.required'=>'Please select a category from dropdown',
        'content.required'=>'Write something on description',
        'type.required'=>'Please select a type from dropdown',
        'title.required'=>'Title is required',
        'tags.required'=>'Tags is required',
        'url.required_if:type,url|url'=>'url is required when type is url',
       );


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function addedBy()
    {
        if ($this->added_by_type === 'admin') {
            return User::find($this->added_by_id);
        }

        return null;
    }

    public function addedUser()
    {
        return $this->belongsTo(User::class, 'added_by_id', 'id');
    }

    public function postNotificationView()
    {
        return $this->hasMany(PostNotificationView::class);
    }

    public static function unseenPosts()
    {
       if (Sentinel::check()) {

            $user = Sentinel::getUser();

            $seen = PostNotificationView::where('user_id', $user->id)->get()->pluck('post_id')->toArray();

            return self::with('addedUser')->whereNotIn('id', $seen)->orderBy('id', 'desc')->get();
       }

       return collect([]);
    }

    public function ratings(){
        return $this->hasMany(PostRate::class);
    }

    public function share(){
        return $this->hasMany(\App\ShareCount::class,'post_id','id');
    }

    public function isUnseen()
    {
        return PostNotificationView::wherePostId($this->id)->exists();
    }

}
