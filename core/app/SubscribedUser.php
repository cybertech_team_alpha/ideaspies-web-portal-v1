<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscribedUser extends Model
{
    protected $fillable = ['user_id','email'];
}
