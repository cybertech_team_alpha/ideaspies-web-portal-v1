<?php
namespace ProductManage\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use ProductManage\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Illuminate\Support\Facades\Input;


// use DeviceManage\Models\Device;
use ProductManage\Models\Product;
use OrderManage\Models\Order;
use StockManage\Models\Stock;
use ProductManage\Models\ProductImage;
use ProductManage\Models\DeliveryArea;
use IndustryManage\Models\Industry;
use MerchantManage\Models\MerchantBusiness;
use CategoryManage\Models\Category;
use MerchantManage\Models\location;
use Sentinel;
use Response;
use File;
use Image;
use Config;
use Storage;
use Carbon\Carbon;
use AdManage\Http\Controllers\AdController;


class ProductController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Product Controller
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {


        //$this->middleware('guest');
    }
    /**
     * Show the VIEW NEW PRODUCT MEMBER screen to the user.
     *
     * @return Response
     */

    public function addView()
    {

        $logged_user = Sentinel::getUser();
        $shop_id = 0;
        $pickup_location='';
        if (!$logged_user->inRole('admin')) {
            $shop_id = MerchantBusiness::where('personal_id',Sentinel::getUser()->id)->first()->id;
            $pickup_location = MerchantBusiness::where('id',$shop_id)->first()->location;
        }
        $industries =Industry::all();
        $locations = location::whereNotNull('district_id')->orderBy('name', 'ASC')->get();

        $load_loactions =[];

        foreach ($locations as $value) {
            $load_loactions[] = array('id' => $value->id,'name'=>$value->name );
        }
        $row = 1;
        $businesses = MerchantBusiness::all();
        return view('ProductManage::back.add')->with([ 'industries' => $industries,'businesses' => $businesses,
            'logged_user'=>$logged_user,'locations'=>$load_loactions,'row'=>$row,'shop_id'=>$shop_id,'pickup_location'=>$pickup_location]);
    }
    /**
     * Show the ADD NEW PRODUCT MEMBER screen to the user.
     *
     * @return Response
     */
    public function addProduct(Request $request)
    {
        $logged_user = Sentinel::getUser();

        if ($request->sale_option =='on') {
            $sale_option = 1;
        }

        if (!$logged_user->inRole('admin')) {
//            $shop_id = MerchantBusiness::where('personal_id',Sentinel::getUser()->id)->first()->id;
            $shop_id = $request->shop_id;
            $shop_name = MerchantBusiness::where('id',$shop_id)->first()->business_name;
            $user_id = Sentinel::getUser()->id;
        }else{
            $shop_id =$request->shop_id;
            $user_id = MerchantBusiness::where('id',$shop_id)->first()->personal_id;
            $shop_name = MerchantBusiness::where('id',$shop_id)->first()->business_name;
        }

        $acronym = "";
        $word='';
        $words = preg_split("/(\s|\-|\.)/", $shop_name);
        $i=0;
        foreach($words as $w) {
            $acronym .= substr($w,0,1);
            if ($i ==1) break;
            $i++;
        }
        $word = $word . $acronym ;

        $product_code_max = Product::where('product_code', 'like', '%'.$word.'%')->orderBy('product_code', 'desc')->first();
        if ($product_code_max){
            $max_code = explode('-',$product_code_max->product_code);
            $invID = $max_code[1] +1;
        } else{
            $invID = 1;
        }

        $product_code = $word .'-'. str_pad($invID, 5, '0', STR_PAD_LEFT);


        if ($request->deli_option =='on') {
            $deli_option = 1;
        }else {
            $deli_option = 0;
        }
        if(!empty($request->delivery_type)){
            $delivery_type = implode(',', $request->delivery_type);
        }else {
            $delivery_type ='';
        }

        if ($request->hot_product =='on') {
            $hot_product = 1;
        }else {
            $hot_product = 0;
        }

        if ($request->allow_others_to_sell =='on') {
            $allow_others_to_sell = 1;
        }else{
            $allow_others_to_sell = 0;
        }
        if ($request->allow_product_return =='on') {
            $allow_product_return = 1;
        }else{
            $allow_product_return = 0;
        }

        if(!empty($request->pickup_areas)){
            $pickup_areas = implode(',', $request->pickup_areas);
        }else {
            $pickup_areas ='';
        }



// dd($request->get('main1_scaled'));

        $product=Product::create([
            'name'=>$request->get( 'name' ),
            'description'=>$request->get( 'description' ),
            'price'=>$request->get( 'price' ),
            'specification'=>$request->get('specification'),
            'saleprice'=>$request->get('saleprice'),
            // 'delivery_price'=>$request->get('delivery_price'),
            'deli_option'=>$deli_option,
            'product_code'=>$product_code,
            'user_id'=>$user_id,
            'cat_id'=>$request->get('category'),
            // 'industry_id'=>$request->get('industry_id'),
            'shop_id'=>$shop_id,
            'delivery_type'=>$delivery_type,
            'width'=>$request->width,
            'height'=>$request->height,
            'length'=>$request->length,
            'weight'=>$request->weight,
            'hot_product'=>$hot_product,
            'status'=>1,
            'delivery_details'=>$request->get('delivery_details'),
            'pickup_address'=>$request->get('pickup_address'),
            'allow_product_return' =>$allow_product_return,
            'allow_others_to_sell'=>$allow_others_to_sell,
            'pickup_areas'=>$pickup_areas,
            'return_duration'=>$request->return_duration,
            'delivery_duration'=>$request->delivery_duration

        ]);

        if ($product) {
            $project_file1 = $request->file('product_image1');
            $project_file2 = $request->file('product_image2');

            $sub_image1 = $request->file('product_sup_image1');
            $sub_image2 = $request->file('product_sup_image2');
            $sub_image3 = $request->file('product_sup_image3');

            $path = 'upload/images/products/'.$product->id.'/';
            if (!file_exists(storage_path('upload/images/products/'.$product->id))) {
                File::makeDirectory(storage_path('upload/images/products/'.$product->id));
            }
            $product_image =new ProductImage();
            $product_image->product_id=$product->id;
            $product_image->save();


            $i=0;
            // if ($project_files) {
            //     foreach ($project_files as $key => $project_file) {
            //         if (File::exists($project_file)) {
            //             $file = $project_file;
            //             $extn =$file->getClientOriginalExtension();
            //             $project_fileName = 'product-' .Sentinel::getUser()->id.'-'.$product->id.'-'.$i. '.' . $extn;
            //             /*IMAGE UPLOAD*/
            //
            // 						$thumbnailImage = Image::make($file);
            //             $path = 'upload/images/products/';
            //             if (!file_exists(storage_path('upload/images/products'))) {
            //                 File::makeDirectory(storage_path('upload/images/products'));
            //             }
            // 						$thumbnailImage->resize(300,410);
            // 						$destinationPath = storage_path($path);
            // 						$thumbnailImage->save($destinationPath.$project_fileName);
            //             // $file->move($destinationPath, $fileName);
            //
            //             ProductImage::create([
            //                 'product_id'=>$product->id,
            //                 'path'=>$path,
            //                 'filename'=>$project_fileName,
            // 								'type'=>0
            //
            //             ]);
            //             $i++;
            //         }
            //     }
            // }

            if ($project_file1) {

                $scaled = $request->get('main1_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $project_file1;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-1-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);


                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                // $product_image =new ProductImage();

                $product_image->path=$path;
                $product_image->main_image_1 =$project_fileName;
                $product_image->type=0;
                $product_image->save();

            }

            if ($project_file2) {

                $scaled = $request->get('main2_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $project_file2;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-2-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                // $product_image =new ProductImage();

                // $product_image->path=$path;
                $product_image->main_image_2 =$project_fileName;
                $product_image->type=0;
                $product_image->save();

            }

            //sub images
            if ($sub_image1) {

                $scaled = $request->get('sub1_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $sub_image1;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-sub-1-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                $product_image->sub_image_1 =$project_fileName;
                $product_image->save();

            }

            if ($sub_image2) {

                $scaled = $request->get('sub2_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $sub_image2;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-sub-2-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);
                // $path = 'upload/images/products/';
                //
                // if (!file_exists(storage_path('upload/images/products'))) {
                // 		File::makeDirectory(storage_path('upload/images/products'));
                // }

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                $product_image->sub_image_2 =$project_fileName;
                $product_image->save();

            }

            if ($sub_image3) {

                $scaled = $request->get('sub3_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $sub_image3;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-sub-3-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);
                // $path = 'upload/images/products/';
                //
                // if (!file_exists(storage_path('upload/images/products'))) {
                // 		File::makeDirectory(storage_path('upload/images/products'));
                // }

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                $product_image->sub_image_3 =$project_fileName;
                $product_image->save();

            }



            //delivery Area insert
            if ($deli_option == 1) {
                $row =$request->dataRow;
                $save_row_num = 0;

                for ($i=0; $i <= $row; $i++) {
                    $delivery_price = 'delivery_price'.$i;
                    $delivery_area = 'delivery_area'.$i;
                    if (!empty($request->$delivery_area)) {

                        foreach ($request->$delivery_area as $area){
                            DeliveryArea::create([
                                'product_id'=>$product->id,
                                'location_id'=>$area,
                                'price'=>$request->$delivery_price[0],
                                'row_num'=>$save_row_num,
                                'delivery_type'=>1

                            ]);
                        }
                        $save_row_num++;

                    }
                }


                if (!empty($request->free_delivery)) {
                    $delivery_area =$product->getFreeLocations;
                    if ($delivery_area){
                        foreach ($delivery_area as $area){
                            $area->delete();
                        }
                    }
                    foreach ($request->free_delivery as $area){
                        DeliveryArea::create([
                            'product_id'=>$product->id,
                            'location_id'=>$area,
                            'price'=>0,
                            'delivery_type'=>2
                        ]);
                    }
                }

                if (!empty($request->non_delivery)) {
                    $delivery_area =$product->getNonLocations;
                    if ($delivery_area){
                        foreach ($delivery_area as $area){
                            $area->delete();
                        }
                    }
                    foreach ($request->non_delivery as $area){
                        DeliveryArea::create([
                            'product_id'=>$product->id,
                            'location_id'=>$area,
                            'price'=>0,
                            'delivery_type'=>3
                        ]);
                    }
                }

            }




            // return redirect('product/add')->with(['success' => true,
            //     'success.message' => 'Product Created successfully!',
            //     'success.title' => 'Well Done!']);
            return response()->json(['success' => true,
                'message' => 'Product Created successfully!',
                'title' => 'Well Done!']);
        } else {
            return redirect('product/add')->with([ 'error' => true,
                'error.message'=> 'Product Creation Category Already Exsist!',
                'error.title' => 'Duplicate!']);
        }

    }

    /**
     * Show the VIEW LIST PRODUCT MEMBER screen to the user.
     *
     * @return Response
     */

    public function listView()
    {
        return view( 'ProductManage::back.list' );
    }

    public function jsonList(Request $request)
    {
        $logged_user = Sentinel::getUser();
        if($request->ajax()){
            if ($logged_user->inRole('admin')) {
                $data = Product::get();
            }else {
                $data = Product::where('user_id',Sentinel::getUser()->id)->whereIn('status', [1, 2])->get();
            }
            $jsonList = array();
            $i=1;
            foreach ($data as $key => $product) {
                $dd = array();
                array_push($dd, $i);

                array_push($dd, $product->product_code);
                array_push($dd, $product->name);
                array_push($dd, $product->price);
                array_push($dd, $product->saleprice);
                array_push($dd, '<label class="switch"> <input type="checkbox" value="'.$product->id.'" class="check-active-deactivate" '.(($product->status ==1) ? "checked" : "").'> <span class="slider round "></span></label>');
                array_push($dd, '<a href="#" class="blue" onclick="window.location.href=\''.url('product/edit/'.$product->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit Product"><i class="fa fa-pencil"></i></a>');
                array_push($dd, '<a href="#" class="red product-delete" data-id="'.$product->id.'" data-toggle="tooltip" data-placement="top" title="Delete Product"><i class="fa fa-trash-o"></i></a>');


                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data'=>$jsonList));
        }else{
            return Response::json(array('data'=>[]));
        }

    }

    public function editView($id)
    {
        $industries =Industry::all();
        $businesses = MerchantBusiness::all();
        $locations = location::whereNotNull('district_id')->orderBy('name', 'ASC')->get();
        $load_loactions =[];

        // $all = array('id' =>'0','name'=>'All Others' );
        // array_push($load_loactions,$all);
        foreach ($locations as $value) {
            $load_loactions[] = array('id' => $value->id,'name'=>$value->name );
        }
        // dd($load_loactions);
        $logged_user = Sentinel::getUser();

        if ($logged_user->inRole('admin') OR $logged_user->inRole('agent-admin')) {

            $product=Product::with(['getImages'])->find($id);
        } else {
            $product=Product::with(['getImages'])->where('user_id',Sentinel::getUser()->id)->find($id);
        }
        // $main_category=$this->category_load();
        $delivery_type = array();
        if (!empty($product->delivery_type)) {
            $delivery_type = explode(',', $product->delivery_type);
        }

        $pickup_areas = array();
        if (!empty($product->pickup_areas)) {
            $pickup_areas = explode(',', $product->pickup_areas);
        }


        if($product){
            $delivery_areas = $product->getLocations;
            // if (empty($delivery_areas)){
            $selected_ids=[];
            $row_nums=[];
            $delivery_price=[];
            $free_delivery=[];
            $non_delivery=[];
            $row ='';
            // }
            if ($product->deli_option != 0) {
                foreach ($delivery_areas as $area){
                    $selected_ids[$area->row_num][] = $area->location_id;
                    $row_nums[] = $area->row_num;
                    $delivery_price[$area->row_num][] = $area->price;
                }

            }else{
                $row = 1;
            }
            foreach ($product->getFreeLocations as $area){
                $free_delivery[] = $area->location_id;
            }
            foreach ($product->getNonLocations as $area){
                $non_delivery[] = $area->location_id;
            }


            sort($row_nums);

            if (empty($row_nums)) {
                $selected_ids[0][]=0;
                $row_nums[] = 0;
                $delivery_price[0][] = 0;
            }
            $proEdit = $product->checkAdEditing(request()->_token);
            if ($proEdit && isset($proEdit['editing'])) {
                return redirect()->back()->with(['warning' => true,
                    'warning.message' => $proEdit['editing'],
                    'warning.title' => 'Please try again later']);
                // return redirect()->back()->withErrors(['editing', $adEdit['editing']]);
            }

            $image_url_array=[];
            $image_config_array=[];
            $sup_image_url_array=[];
            $sup_image_config_array=[];

            $images = $product->getImages;
            // dd($delivery_type);
            return view( 'ProductManage::back.edit' )->with(['product'=>$product,'images'=>$image_url_array,'sup_images' =>$sup_image_url_array,
                'image_config'=>$image_config_array,'sup_image_config'=>$sup_image_config_array,'industries'=>$industries,'businesses'=>$businesses,
                'delivery_type'=>$delivery_type,'logged_user'=>$logged_user,'locations'=>$load_loactions,'free_delivery'=>$free_delivery,'non_delivery'=>$non_delivery,
                'delivery_areas'=>$selected_ids,'row_nums'=>array_unique($row_nums),
                'delivery_price' =>$delivery_price,'product_images'=>$images,'single_row'=>$row,'pickup_areas'=>$pickup_areas]);
        }else{
            return view( 'errors.404' );
        }
    }

    public function editProduct($id,Request $request)
    {

        $logged_user = Sentinel::getUser();

        if ($logged_user->inRole('admin')) {

            $product=Product::find($id);
        } else {
            $product=Product::where('user_id',Sentinel::getUser()->id)->find($id);

        }

        $shop_id =$request->shop_id;
        $user_id = MerchantBusiness::where('id',$shop_id)->first()->personal_id;

        if($product){

            if ($request->sale_option =='on') {
                $sale_option = 1;
            }
            // $delivery_type = implode(',', $request->delivery_type);
            // dd($delivery_type);

            if ($request->deli_option =='on') {
                $deli_option = 1;
                $delivery_duration=$request->delivery_duration;
            }else {
                $deli_option = 0;
                $delivery_duration='';
            }
            if(!empty($request->delivery_type)){
                $delivery_type = implode(',', $request->delivery_type);
            }else {
                $delivery_type ='';
            }
            if ($request->hot_product =='on') {
                $hot_product = 1;
            }else {
                $hot_product = 0;
            }
            if ($request->allow_others_to_sell =='on') {
                $allow_others_to_sell = 1;
            }else{
                $allow_others_to_sell = 0;
            }
            if ($request->allow_product_return =='on') {
                $allow_product_return = 1;
                $return_duration=$request->return_duration;
            }else{
                $allow_product_return = 0;
                $return_duration='';
            }

            if(!empty($request->pickup_areas)){
                $pickup_areas = implode(',', $request->pickup_areas);
            }else {
                $pickup_areas ='';
            }

            $product->name=$request->get( 'name' );
            $product->description=$request->get( 'description' );
            $product->specification=$request->get( 'specification' );
            $product->saleprice=$request->get( 'saleprice' );
            // $product->delivery_price=$request->get( 'delivery_price' );
            $product->price=$request->get( 'price' );
            $product->cat_id=$request->get('category');
            $product->shop_id=$shop_id;
            $product->user_id=$user_id;
            $product->hot_product=$hot_product;
            $product->deli_option=$deli_option;
            $product->delivery_type=$delivery_type;
            $product->width=$request->width;
            $product->height=$request->height;
            $product->length=$request->length;
            $product->weight=$request->weight;
            $product->delivery_details=$request->get('delivery_details');
            $product->pickup_address=$request->get('pickup_address');
            $product->allow_product_return=$allow_product_return;
            $product->allow_others_to_sell=$allow_others_to_sell;
            $product->pickup_areas=$pickup_areas;
            $product->return_duration=$return_duration;
            $product->delivery_duration=$delivery_duration;
            // $product->status=1;
            $product->save();

//             dd($product);

            //COMMENTED by Krish , please check this line because product update not working
            // $productUpdatedColumns = $product->getDirty();
            // $array_keys = array_keys($productUpdatedColumns);

            $project_file1 = $request->file('product_image1');
            $project_file2 = $request->file('product_image2');
            $sub_image1 = $request->file('product_sup_image1');
            $sub_image2 = $request->file('product_sup_image2');
            $sub_image3 = $request->file('product_sup_image3');

            if ($product) {
                $product_image = ProductImage::where('product_id',$product->id)->first();
                if (empty($product_image)) {
                    $product_image = ProductImage::create([
                        'product_id'=>$product->id,
                    ]);
                }
            }

            $path = 'upload/images/products/'.$product->id.'/';
            if (!file_exists(storage_path('upload/images/products/'.$product->id))) {
                File::makeDirectory(storage_path('upload/images/products/'.$product->id));
            }
            if ($project_file1) {

                $scaled = $request->get('main1_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $project_file1;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-1-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);


                $product_image->path=$path;
                $product_image->main_image_1 =$project_fileName;
                $product_image->type=0;
                $product_image->save();

            }

            if ($project_file2) {

                $scaled = $request->get('main2_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $project_file2;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-2-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                $product_image->path=$path;
                $product_image->main_image_2 =$project_fileName;
                $product_image->type=0;
                $product_image->save();

            }

            //sub images
            if ($sub_image1) {

                $scaled = $request->get('sub1_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $sub_image1;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-sub-1-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                $product_image->sub_image_1 =$project_fileName;
                $product_image->save();

            }

            if ($sub_image2) {

                $scaled = $request->get('sub2_scaled');
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $sub_image2;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-sub-2-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                $product_image->sub_image_2 =$project_fileName;
                $product_image->save();

            }

            if ($sub_image3) {

                $scaled = $request->get('sub3_scaled');
                // dd($scaled);
                $width = isset($scaled['width']) ? $scaled['width'] : 0;
                $height = isset($scaled['height']) ? $scaled['height'] : 0;
                $x = isset($scaled['x']) ? $scaled['x'] : 0;
                $y = isset($scaled['y']) ? $scaled['y'] : 0;

                $file = $sub_image3;
                $extn =$file->getClientOriginalExtension();
                $project_fileName = 'product-sub-3-' .Sentinel::getUser()->id.'-'.$product->id.'.' . $extn;
                /*IMAGE UPLOAD*/
                $thumbnailImage = Image::make($file);
                // $path = 'upload/images/products/';
                //
                if (file_exists(storage_path($path.$project_fileName))) {
                    // Storage::delete($path.$project_fileName);
                    unlink(storage_path($path.$project_fileName));
                }

                //cropImage
                $destinationPath = storage_path($path);
                if($width && $height) {
                    $thumbnailImage->crop($width, $height, $x, $y);
                }
                $thumbnailImage->save($destinationPath.$project_fileName,60);

                $product_image->sub_image_3 =$project_fileName;
                $product_image->save();

            }


            $delivery_area =$product->getLocations;
            if ($delivery_area){
                foreach ($delivery_area as $area){
                    $area->delete();
                }
            }

            if ($deli_option == 1) {
                // $arrayName = array();
                $row =$request->dataRow;
                $save_row_num = 0;
                for ($i=0; $i <= $row; $i++) {
                    $delivery_price = 'delivery_price'.$i;
                    $delivery_area_select = 'delivery_area'.$i;
                    if (!empty($request->$delivery_area_select)) {

                        // $arrayName[] = $request->$delivery_area_select;
                        foreach ($request->$delivery_area_select as $area){
                            DeliveryArea::create([
                                'product_id'=>$product->id,
                                'location_id'=>$area,
                                'price'=>$request->$delivery_price[0],
                                'row_num'=>$save_row_num,
                                'delivery_type'=>1

                            ]);
                        }
                        $save_row_num++;
                    }
                }

                // dd($arrayName);

                if (!empty($request->free_delivery)) {
                    $delivery_area =$product->getFreeLocations;
                    if ($delivery_area){
                        foreach ($delivery_area as $area){
                            $area->delete();
                        }
                    }
                    foreach ($request->free_delivery as $area){
                        DeliveryArea::create([
                            'product_id'=>$product->id,
                            'location_id'=>$area,
                            'price'=>0,
                            'delivery_type'=>2
                        ]);
                    }
                }

                if (!empty($request->non_delivery)) {
                    $delivery_area =$product->getNonLocations;
                    if ($delivery_area){
                        foreach ($delivery_area as $area){
                            $area->delete();
                        }
                    }
                    foreach ($request->non_delivery as $area){
                        DeliveryArea::create([
                            'product_id'=>$product->id,
                            'location_id'=>$area,
                            'price'=>0,
                            'delivery_type'=>3
                        ]);
                    }
                }
            }



            // return redirect( 'product/edit/'.$id )->with([ 'success' => true,
            // 	'success.message'=> 'Product updated successfully!',
            // 	'success.title' => 'Good Job!' ]);
            return response()->json(['success' => true,
                'message' => 'Product updated successfully!',
                'title' => 'Good Job!']);

        }else{
            return view( 'errors.404' );
        }
    }

    public function getCatRate(Request $request){
        $category_id =$request->get('category_id');

        $rate = Category::where('id',$category_id)->first()->rate;
        if ($rate =='')$rate=0;

        return Response::json(array('rate'=>$rate));
    }

    public function productImageDelete(Request $request)
    {
        $image_id=$request->get('key');
        $image=ProductImage::find($image_id);
        if ($image) {
            // Storage::delete('/core/storage/'.$image->path.$image->filename);
            // File::delete('/core/storage/'.$image->path.$image->filename);
            \File::delete(storage_path($image->path.$image->filename));

        }
        $image->delete();
        return 1;
    }

    public function deleteProduct(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $product = Product::find($id);
            if($product){
                $product->status=4;
                $product->deleted_at= Carbon::now();
                $product->update();

                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function remove_delivery_areas(Request $request)
    {
        $product_id =$request->get('product_id');
        $delivery_area =$request->get('delivery_area');
        $areas = DeliveryArea::where('product_id',$product_id)->get();
        // dd($areas);

        foreach ($areas as $area) {

            if (in_array($area->location_id,$delivery_area)) {
                $delete = DeliveryArea::where('id',$area->id)->delete();
            }
        }
        return response()->json([
            'succes'=>true,
            'msg'=>'Removed successfully',
            'title' => 'Good Job!'
        ]);
    }

    public function productActivateDeactivate(Request $request)
    {
        $id = $request->id;
        $check = $request->check;
        $product = Product::find($id);
        // dd($product);
        if($product){
            if ($check == 1) {
                // activate
                $product->status = 1;
                $product->save();
                return response()->json([
                    'succes'=>true,
                    'msg'=>'Activated product '.$product->name,
                    'title' => 'Good Job!'
                ]);

            }else {
                $product->status = 2;
                $product->save();
                return response()->json([
                    'succes'=>true,
                    'msg'=>'Deactivated product '.$product->name,
                    'title' => 'Good Job!'
                ]);
            }

        }
    }

    public function getAjaxLocation(Request $request)
    {
        $selected_location =$request->get('selected_location');
        $search =$request->get('searchTerm');
        $locations = location::whereNotNull('district_id')->whereNotIn('id',$selected_location)
            ->orderBy('name', 'ASC')
             ->where('name', 'like', '%'.$search.'%')
            ->get();
        // print_r($locations);

        $load_loactions =[];
        foreach ($locations as $value) {
            $load_loactions[] = array('id' => $value->id,'text'=>$value->name );
        }

        return Response::json($load_loactions);

    }

    public function get_merchant_pickup(Request $request){
        $shop_id =$request->get('shop_id');

        $data = MerchantBusiness::where('id',$shop_id)->first();

        return Response::json(array('location'=>$data->location));
    }




    public function deactivateProduct(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $product = Product::find($id);
            if($product){
                $product->status = 4;
                $product->save();

                // activity
                parent::activity_create('Deactivated product '.$product->name);
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }
    public function reactivateProduct(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $product = Product::find($id);
            if($product){
                $product->status = 0;
                $product->save();

                // activity
                parent::activity_create('Reactivated product '.$product->name);
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /*PRODUCT APPROVE */

    public function approveListView()
    {
        parent::activity_create('Visited <a href="'.url("product/approve/list").'">Product Approve List</a>');
        return view( 'ProductManage::back.approval.list' );
    }
    public function approveJsonList(Request $request)
    {
        if($request->ajax()){
            $user=Sentinel::getUser();
            if (Sentinel::findRoleById(4)) {
                $data= Product::with(['getUser'])->where('status','=',0)->orderby('id','desc')->get();
            } else {
                $data= Product::with(['getUser'])->where('user_id',(Sentinel::getUser()->id))->where('status','=',0)->orderby('id','desc')->get();
            }

            $jsonList = array();
            $i=1;
            foreach ($data as $key => $product) {

                $dd = array();
                array_push($dd, $i);

                if($product->name != ""){
                    array_push($dd, $product->name);
                }else{
                    array_push($dd, "-");
                }
                if($product->price != ""){
                    array_push($dd, number_format($product->price,2));
                }else{
                    array_push($dd, "-");
                }
                if($product->getUser != ""){
                    array_push($dd, $product->getUser->first_name.' '.$product->getUser->last_name);
                }else{
                    array_push($dd, "-");
                }

                if (Sentinel::findRoleById(4)) {
                    array_push($dd, '<center><span class="label label-primary pull-right">Pending</span></center>
						<div class="fixed_float">
						<div class="btn-group" role="group">
						<a href="'.url("product/approve/detail/".$product->id).'"><button type="button" class="btn btn-secondary ad-view blue" title="View"><i class="fa fa-eye"></i></button></a>
						<a href="'.url("product/edit/".$product->id).'"><button type="button" class="btn btn-secondary ad-view green" title="Edit"><i class="fa fa-pencil"></i></button></a>
						</div>
						</div>
						');
                } else {
                    array_push($dd, '<center><span class="label label-primary pull-right">Pending</span></center>

						<div class="fixed_float">
						<div class="btn-group" role="group">
						<a href="" class="disabled" data-toggle="tooltip" data-placement="top" title="View Disabled"><button type="button" class="btn btn-secondary ad-view blue" title="View Disabled"><i class="fa fa-eye"></i></button></a>
						<a href="'.url("product/edit/".$product->id).'"><button type="button" class="btn btn-secondary ad-view green" title="Edit"><i class="fa fa-pencil"></i></button></a>
						<a  href=""><button type="button" class="btn btn-secondary ad-view red" title="Deactivate" onclick="deleteProduct('.$product->id.')"><i class="fa fa-trash"></i></button></a>
						</div>
						</div>
						');
                }
                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data'=>$jsonList));
        }else{
            return Response::json(array('data'=>[]));
        }
    }
    public function approveDetailView($id)
    {

        $permissions = Permission::whereIn('name',['product.approve','product.reject','admin'])->where('status','=',1)->lists('name');
        if(Sentinel::hasAnyAccess($permissions)){
            $product=Product::with(['getImages','getUser','getBuninessDetails'])->find($id);
            //activity
            parent::activity_create('Visited <a href="'.url("product/approve/detail/".$id).'">'.$product->name .' Approve Details</a>');
            return view( 'ProductManage::back.approval.details' )->with(['products'=>$product]);
        }else{
            return view( 'errors.404' );
        }

    }
    public function approveProduct(Request $request)
    {

        $AdController = new AdController();

        $short_code = $AdController->getAdCode();
        $db_adcode = Product::where('short_code', $short_code)->first();

        if($db_adcode !== null){
            $short_code = $this->getAdCode();
        }

        $product_id=$request->get('id');
        // activity
        parent::activity_create('Approved product '.Product::find($product_id)->name);
        return $result=Product::where('id', $product_id)
            ->update(['status' => 1, 'short_code' => $short_code]);
    }
    public function rejectProduct(Request $request)
    {
        $product_id=$request->get('id');
        // activity
        parent::activity_create('Rejected product '.Product::find($product_id)->name);
        return $result=Product::where('id', $product_id)
            ->update(['status' => 2]);
    }

    /**
     * Show the VIEW ADD PRODUCT MEMBER screen to the user.
     *
     * @return Response
     */

    public function viewAddProductMember_Front()
    {
        return view('ProductManage::front.addProductMember');
    }


}
